﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using Placeme.Common.Application;

namespace Placeme.Common.Tests.Application
{
	[TestFixture]
	public class PlacemeUrlProviderTests
	{
		[OneTimeSetUp]
		public void Setup()
		{
			var myConfiguration = new Dictionary<string, string> {{ "ROOT_URL", "https://placeme.ch" }};

			_config = new ConfigurationBuilder()
				.AddInMemoryCollection(myConfiguration)
				.Build();
		}

		private IConfiguration _config;

		[Test]
		public void ResolvesUrlForLoginClient()
		{
			var urlProvider = new PlacemeUrlProvider(_config);
			Assert.That(urlProvider.LoginClient, Is.EqualTo(new Uri("https://login.placeme.ch")));
		}

		[Test]
		public void HttpsCanBeDisabled()
		{
			var myConfiguration = new Dictionary<string, string> {{"ROOT_URL", "http://placeme.ch"}};

			var config = new ConfigurationBuilder()
				.AddInMemoryCollection(myConfiguration)
				.Build();

			var urlProvider = new PlacemeUrlProvider(config);
			Assert.That(urlProvider.LoginClient, Is.EqualTo(new Uri("http://login.placeme.ch")));
		}

		[Test]
		public void CanHandlePorts()
		{
			var myConfiguration = new Dictionary<string, string> {{"ROOT_URL", "http://placeme.ch:6969"}};

			var config = new ConfigurationBuilder()
				.AddInMemoryCollection(myConfiguration)
				.Build();

			var urlProvider = new PlacemeUrlProvider(config);
			Assert.That(urlProvider.LoginClient, Is.EqualTo(new Uri("http://login.placeme.ch:6969")));
		}

		[Test]
		public void CanBeForcedToUseDevelopmentUrls()
		{
			var myConfiguration = new Dictionary<string, string>
			{
				{"ROOT_URL", "http://placeme.ch:6969"},
				{"CURRENT_USER_FROM_CONFIG", "true"},
			};

			var config = new ConfigurationBuilder()
				.AddInMemoryCollection(myConfiguration)
				.Build();

			var urlProvider = new PlacemeUrlProvider(config);
			Assert.That(urlProvider.LoginClient, Is.EqualTo(new Uri("http://placeme.ch:6969")));
			Assert.That(urlProvider.Oidc, Is.EqualTo(new Uri("http://placeme.ch:6969")));
			Assert.That(urlProvider.Iam, Is.EqualTo(new Uri("http://placeme.ch:6969")));
			Assert.That(urlProvider.Portal, Is.EqualTo(new Uri("http://placeme.ch:6969")));
			Assert.That(urlProvider.PortalApi, Is.EqualTo(new Uri("http://placeme.ch:6969")));
		}

		[Test]
		public void ResolvesUrlForPortalApi()
		{
			var urlProvider = new PlacemeUrlProvider(_config);
			Assert.That(urlProvider.PortalApi, Is.EqualTo(new Uri("https://api.portal.placeme.ch")));
		}

		[Test]
		public void ResolvesUrlForPortalClient()
		{
			var urlProvider = new PlacemeUrlProvider(_config);
			Assert.That(urlProvider.Portal, Is.EqualTo(new Uri("https://portal.placeme.ch")));
		}

		[Test]
		public void ResolvesUrlForIam()
		{
			var urlProvider = new PlacemeUrlProvider(_config);
			Assert.That(urlProvider.Iam, Is.EqualTo(new Uri("https://iam.placeme.ch")));
		}

		[Test]
		public void ResolvesUrlForOidc()
		{
			var urlProvider = new PlacemeUrlProvider(_config);
			Assert.That(urlProvider.Oidc, Is.EqualTo(new Uri("https://oidc.placeme.ch")));
		}
	}
}