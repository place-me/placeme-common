using System;
using System.Threading;
using System.Threading.Tasks;
using EntityFrameworkCore.DbContextScope;
using Moq;
using NUnit.Framework;
using Placeme.Common.Application.Commands.Delete;
using Placeme.Common.Application.Exceptions;
using Placeme.Common.Identity;
using Placeme.Common.Persistence;
using Placeme.Common.Tests.Application.Commands.Mocks;

namespace Placeme.Common.Tests.Application.Commands
{
	[TestFixture]
	public class DeleteCommandTests
	{
		private IDbContextScopeFactory _dbContextScopeFactory;
		private IRepository<FakeEntity> _repository;
		private readonly FakeMapper _mapper = new();

		[OneTimeSetUp]
		public void Setup()
		{
			var dbContextScopeMock = new Mock<IDbContextScope>();
			dbContextScopeMock.Setup(_ => _.SaveChangesAsync(It.IsAny<CancellationToken>()));
			var dbContextMock = new Mock<IDbContextScopeFactory>();
			dbContextMock.Setup(_ => _.Create(It.IsAny<DbContextScopeOption>())).Returns(dbContextScopeMock.Object);

			var repositoryMock = new Mock<IRepository<FakeEntity>>();
			repositoryMock.Setup(_ => _.GetAsync(It.IsAny<Guid>(), It.IsAny<bool>(), It.IsAny<CancellationToken>())).ReturnsAsync(new FakeEntity
			{
				Foobar = "blubber"
			});
			var currentUserMock = new Mock<ICurrentUser>();

			_dbContextScopeFactory = dbContextMock.Object;
			_repository = repositoryMock.Object;
		}

		[Test]
		public async Task EnsureCorrectOutput()
		{
			var handler = new DeleteCommandHandler<FakeDto, FakeEntity>(_dbContextScopeFactory, _repository, _mapper);
			var command = new DeleteCommand<FakeDto>
			{
				Id = Guid.NewGuid()
			};

			var dto = await handler.Handle(command, default);
			Assert.That(dto.Foobar, Is.EqualTo("blubber"));
		}

		[Test]
		public void EnsureExceptionOnInvalidId()
		{
			var repositoryMock = new Mock<IRepository<FakeEntity>>();
			repositoryMock.Setup(_ => _.GetAsync(It.IsAny<Guid>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()));
			var handler = new DeleteCommandHandler<FakeDto, FakeEntity>(_dbContextScopeFactory, repositoryMock.Object, _mapper);
			var command = new DeleteCommand<FakeDto>
			{
				Id = Guid.NewGuid()
			};
			Assert.ThrowsAsync<NotFoundException>(async () => await handler.Handle(command, default));
		}
	}
}