﻿using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using EntityFrameworkCore.DbContextScope;
using Moq;
using NUnit.Framework;
using Placeme.Common.Application.Commands.GetById;
using Placeme.Common.Persistence;
using Placeme.Common.Tests.Application.Commands.Mocks;

namespace Placeme.Common.Tests.Application.Commands
{
	[TestFixture]
	public class GetCommandTests
	{
		private IDbContextScopeFactory _dbContextScopeFactory;
		private IRepository<FakeEntity> _repository;

		[OneTimeSetUp]
		public void Setup()
		{
			var dbContextScopeMock = new Mock<IDbContextScope>();
			dbContextScopeMock.Setup(_ => _.SaveChangesAsync(It.IsAny<CancellationToken>()));

			var dbContextFactoryMock = new Mock<IDbContextScopeFactory>();
			dbContextFactoryMock.Setup(_ => _.Create(It.IsAny<DbContextScopeOption>())).Returns(dbContextScopeMock.Object);

			var entity = new FakeEntity { Foobar = "blubber" };

			var repositoryMock = new Mock<IRepository<FakeEntity>>();
			repositoryMock
				.Setup(_ => _.GetAsync<FakeDto>(It.IsAny<Guid>(), It.IsAny<bool>(), It.IsAny<CancellationToken>(), It.IsAny<Expression<Func<FakeEntity, object>>[]>()))
				.ReturnsAsync(new FakeDto { Foobar = "blubber" });

			_dbContextScopeFactory = dbContextFactoryMock.Object;
			_repository = repositoryMock.Object;
		}

		// #############################################################
		// Todo -> Write tests for get all queries

		[Test]
		public async Task GetByIdEnsureCorrectOutput()
		{
			var handler = new GetByIdQueryHandler<FakeDto, FakeEntity>(_dbContextScopeFactory, _repository);
			var command = new GetByIdQuery<FakeDto>
			{
				Id = Guid.NewGuid()
			};

			var dto = await handler.Handle(command, default);
			Assert.AreEqual("blubber", dto.Foobar);
		}

		[Test]
		[Ignore("To be defined")]
		public void GetAllEnsureCorrectOutput()
		{
			// var handler = new GetAllQueryHandler<FakeDto, FakeEntity>(_dbContextScopeFactory, _repository);
			// var command = new GetAllQuery<FakeDto>();

			// var dto = await handler.Handle(command, default);
			// Assert.AreEqual("blubber", dto.Foobar);
		}
	}
}