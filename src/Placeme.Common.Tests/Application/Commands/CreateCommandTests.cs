using System;
using System.Threading;
using System.Threading.Tasks;

using EntityFrameworkCore.DbContextScope;

using Moq;

using NUnit.Framework;

using Placeme.Common.Application.Commands;
using Placeme.Common.Application.Commands.Create;
using Placeme.Common.Identity;
using Placeme.Common.Persistence;
using Placeme.Common.Tests.Application.Commands.Mocks;

namespace Placeme.Common.Tests.Application.Commands
{
	[TestFixture]
	public class CreateCommandTests
	{
		private IDbContextScopeFactory _dbContextScopeFactory;
		private IRepository<FakeEntity> _repository;
		private ICurrentUser _currentUser;
		private readonly FakeMapper _mapper = new();

		[OneTimeSetUp]
		public void Setup()
		{
			var dbContextScopeMock = new Mock<IDbContextScope>();
			dbContextScopeMock.Setup(_ => _.SaveChangesAsync(It.IsAny<CancellationToken>()));
			var dbContextMock = new Mock<IDbContextScopeFactory>();
			dbContextMock.Setup(_ => _.Create(It.IsAny<DbContextScopeOption>())).Returns(dbContextScopeMock.Object);
			var repositoryMock = new Mock<IRepository<FakeEntity>>();
			repositoryMock.Setup(_ => _.AddAsync(It.IsAny<FakeEntity>(), It.IsAny<CancellationToken>()));
			var currentUserMock = new Mock<ICurrentUser>();

			_dbContextScopeFactory = dbContextMock.Object;
			_repository = repositoryMock.Object;
			_currentUser = currentUserMock.Object;
		}

		[TearDown]
		public void Teardown()
		{
			FakeDto.AfterMap = null;
		}

		[Test]
		public async Task EnsureCurrentUserInMappingOptions()
		{
			FakeDto.AfterMap = (_, _, opts) =>
			{
				Assert.NotNull(opts.Options.Items);
				Assert.True(opts.Options.Items.ContainsKey(Constants.CurrentUserItem));
			};
			var handler = new CreateCommandHandler<FakeDto, FakeEntity>(_dbContextScopeFactory, _repository, _mapper, _currentUser);
			var command = new CreateCommand<FakeDto>
			{
				Data = new FakeDto
				{
					Foobar = "blubber"
				}
			};

			await handler.Handle(command, default);
		}

		[Test]
		public async Task EnsureCorrectOutput()
		{
			var handler = new CreateCommandHandler<FakeDto, FakeEntity>(_dbContextScopeFactory, _repository, _mapper, _currentUser);
			var command = new CreateCommand<FakeDto>
			{
				Data = new FakeDto
				{
					Foobar = "blubber"
				}
			};

			var dto = await handler.Handle(command, default);
			Assert.AreEqual("blubber", dto.Foobar);
		}
	}
}