using System;
using System.Threading;
using System.Threading.Tasks;

using EntityFrameworkCore.DbContextScope;

using Google.Protobuf.WellKnownTypes;

using Moq;

using NUnit.Framework;

using Placeme.Common.Application.Commands;
using Placeme.Common.Application.Commands.Update;
using Placeme.Common.Application.Exceptions;
using Placeme.Common.Identity;
using Placeme.Common.Persistence;
using Placeme.Common.Tests.Application.Commands.Mocks;
using Placeme.Common.Tests.Generated.Base;

namespace Placeme.Common.Tests.Application.Commands
{
	[TestFixture]
	public class UpdateCommandTests
	{
		private IDbContextScopeFactory _dbContextScopeFactory;
		private IRepository<FakeEntity> _repository;
		private ICurrentUser _currentUser;
		private readonly FakeMapper _mapper = new();
		private FakeEntity _fakeEntity;

		[SetUp]
		public void Setup()
		{
			var dbContextScopeMock = new Mock<IDbContextScope>();
			dbContextScopeMock.Setup(_ => _.SaveChangesAsync(It.IsAny<CancellationToken>()));
			var dbContextMock = new Mock<IDbContextScopeFactory>();
			dbContextMock.Setup(_ => _.Create(It.IsAny<DbContextScopeOption>())).Returns(dbContextScopeMock.Object);
			var repositoryMock = new Mock<IRepository<FakeEntity>>();
			_fakeEntity = new FakeEntity
			{
				Foobar = "hello world"
			};
			repositoryMock.Setup(_ => _.GetAsync(It.IsAny<Guid>(), It.IsAny<bool>(), It.IsAny<CancellationToken>())).ReturnsAsync(_fakeEntity);
			var currentUserMock = new Mock<ICurrentUser>();

			_dbContextScopeFactory = dbContextMock.Object;
			_repository = repositoryMock.Object;
			_currentUser = currentUserMock.Object;
		}

		[TearDown]
		public void Teardown()
		{
			FakeMessage.AfterMap = null;
		}

		[Test]
		public async Task EnsureCurrentUserInMappingOptions()
		{
			FakeMessage.AfterMap = (_, _, opts) =>
			{
				Assert.NotNull(opts.Options.Items);
				Assert.True(opts.Options.Items.ContainsKey(Constants.CurrentUserItem));
			};
			var handler = new UpdateCommandHandler<FakeMessage, FakeEntity>(_dbContextScopeFactory, _repository, _mapper, _currentUser);
			var command = new UpdateCommand<FakeMessage>(new FakeMessage
			{
				Foobar = "blubber"
			}, new FieldMask());

			await handler.Handle(command, default);
		}

		[Test]
		public async Task EnsureCorrectOutput()
		{
			var handler = new UpdateCommandHandler<FakeMessage, FakeEntity>(_dbContextScopeFactory, _repository, _mapper, _currentUser);
			var command = new UpdateCommand<FakeMessage>(new FakeMessage
			{
				Foobar = "blubber"
			}, new FieldMask());

			var dto = await handler.Handle(command, default);
			Assert.That(dto.Foobar, Is.EqualTo("hello world"));
		}

		[Test]
		public async Task EnsureCorrectMaskedOutput()
		{
			var handler = new UpdateCommandHandler<FakeMessage, FakeEntity>(_dbContextScopeFactory, _repository, _mapper, _currentUser);
			var fieldMask = new FieldMask();
			fieldMask.Paths.Add("foobar");
			var command = new UpdateCommand<FakeMessage>(new FakeMessage
			{
				Foobar = "blubber",
				Foobarer = "Test123",
				Barfoo = 10
			}, fieldMask);

			var dto = await handler.Handle(command, default);
			Assert.That(dto.Foobar, Is.EqualTo("blubber"));
			Assert.That(dto.Foobarer, Is.EqualTo(string.Empty));
			Assert.That(dto.Barfoo, Is.EqualTo(0));
		}

		[Test]
		public async Task EnsureCorrectMaskedOutputMultiple()
		{
			var handler = new UpdateCommandHandler<FakeMessage, FakeEntity>(_dbContextScopeFactory, _repository, _mapper, _currentUser);
			var fieldMask = new FieldMask();
			fieldMask.Paths.Add("foobar");
			fieldMask.Paths.Add("barfoo");
			var command = new UpdateCommand<FakeMessage>(new FakeMessage
			{
				Foobar = "blubber",
				Foobarer = "Test123",
				Barfoo = 10
			}, fieldMask);

			var dto = await handler.Handle(command, default);
			Assert.That(dto.Foobar, Is.EqualTo("blubber"));
			Assert.That(dto.Foobarer, Is.EqualTo(string.Empty));
			Assert.That(dto.Barfoo, Is.EqualTo(10));
		}

		[Test]
		public async Task EnsureCorrectMaskedOutputAll()
		{
			var handler = new UpdateCommandHandler<FakeMessage, FakeEntity>(_dbContextScopeFactory, _repository, _mapper, _currentUser);
			var fieldMask = new FieldMask();
			fieldMask.Paths.Add("foobar");
			fieldMask.Paths.Add("barfoo");
			fieldMask.Paths.Add("foobarer");
			var command = new UpdateCommand<FakeMessage>(new FakeMessage
			{
				Foobar = "blubber",
				Foobarer = "Test123",
				Barfoo = 10
			}, fieldMask);

			var dto = await handler.Handle(command, default);
			Assert.That(dto.Foobar, Is.EqualTo("blubber"));
			Assert.That(dto.Foobarer, Is.EqualTo("Test123"));
			Assert.That(dto.Barfoo, Is.EqualTo(10));
		}

		[Test]
		public void EnsureExceptionOnInvalidId()
		{
			var repositoryMock = new Mock<IRepository<FakeEntity>>();
			repositoryMock.Setup(_ => _.GetAsync(It.IsAny<Guid>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()));
			var handler = new UpdateCommandHandler<FakeMessage, FakeEntity>(_dbContextScopeFactory, repositoryMock.Object, _mapper, _currentUser);
			var command = new UpdateCommand<FakeMessage>(new FakeMessage
			{
				Foobar = "blubber"
			}, new FieldMask());
			Assert.ThrowsAsync<NotFoundException>(async () => await handler.Handle(command, default));
		}
	}
}