using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using Placeme.Common.Tests.Generated.Base;

namespace Placeme.Common.Tests.Application.Commands.Mocks
{
	public class FakeMapper : IMapper
	{
		public IConfigurationProvider ConfigurationProvider { get; }
		public Func<Type, object> ServiceCtor { get; }

		private readonly Mapper _mapper;

		public FakeMapper()
		{
			var configuration = new MapperConfiguration(cfg =>
			{
				cfg.AddProfile<FakeProfile>();
				cfg.AddProfile<FakeMessageProfile>();
			});
			_mapper = new Mapper(configuration);
		}

		public TDestination Map<TDestination>(object source, Action<IMappingOperationOptions<object, TDestination>> opts)
		{
			return _mapper.Map(source, opts);
		}

		public TDestination Map<TSource, TDestination>(TSource source, Action<IMappingOperationOptions<TSource, TDestination>> opts)
		{
			return _mapper.Map(source, opts);
		}

		public TDestination Map<TSource, TDestination>(TSource source, TDestination destination, Action<IMappingOperationOptions<TSource, TDestination>> opts)
		{
			return _mapper.Map(source, destination, opts);
		}

		public object Map(object source, Type sourceType, Type destinationType, Action<IMappingOperationOptions<object, object>> opts)
		{
			return _mapper.Map(source, sourceType, destinationType, opts);
		}

		public object Map(object source, object destination, Type sourceType, Type destinationType, Action<IMappingOperationOptions<object, object>> opts)
		{
			return _mapper.Map(source, destination, sourceType, destinationType, opts);
		}

		public TDestination Map<TDestination>(object source)
		{
			return _mapper.Map<TDestination>(source);
		}

		public TDestination Map<TSource, TDestination>(TSource source)
		{
			return _mapper.Map<TSource, TDestination>(source);
		}

		public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
		{
			return _mapper.Map(source, destination);
		}

		public object Map(object source, Type sourceType, Type destinationType)
		{
			return _mapper.Map(source, sourceType, destinationType);
		}

		public object Map(object source, object destination, Type sourceType, Type destinationType)
		{
			return _mapper.Map(source, destination, sourceType, destinationType);
		}

		public IQueryable<TDestination> ProjectTo<TDestination>(IQueryable source, object parameters = null, params Expression<Func<TDestination, object>>[] membersToExpand)
		{
			throw new NotImplementedException();
		}

		public IQueryable<TDestination> ProjectTo<TDestination>(IQueryable source, IDictionary<string, object> parameters, params string[] membersToExpand)
		{
			throw new NotImplementedException();
		}

		public IQueryable ProjectTo(IQueryable source, Type destinationType, IDictionary<string, object> parameters = null, params string[] membersToExpand)
		{
			throw new NotImplementedException();
		}
	}
}