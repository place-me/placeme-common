using System;
using AutoMapper;
using Placeme.Common.Application.Mappings;
using Placeme.Common.Domain;

namespace Placeme.Common.Tests.Application.Commands.Mocks
{
	public class FakeDto : IMapFrom<FakeEntity>
	{
		public static Action<FakeDto, FakeEntity, ResolutionContext> AfterMap { get; set; }
		public string Foobar { get; set; }

		public void Mapping(Profile profile)
		{
			profile.CreateMap<FakeDto, FakeEntity>().AfterMap((dto, entity, context) => AfterMap?.Invoke(dto, entity, context));
			profile.CreateMap<FakeEntity, FakeDto>();
		}
	}
}