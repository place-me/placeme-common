using AutoMapper;
using Placeme.Common.Domain;
using Placeme.Common.Tests.Generated.Base;

namespace Placeme.Common.Tests.Application.Commands.Mocks
{
	public class FakeMessageProfile : Profile
	{
		public FakeMessageProfile()
		{
			new FakeMessage().Mapping(this);
		}
	}
}