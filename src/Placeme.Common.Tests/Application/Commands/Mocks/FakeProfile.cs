using AutoMapper;
using Placeme.Common.Domain;
using Placeme.Common.Tests.Generated.Base;

namespace Placeme.Common.Tests.Application.Commands.Mocks
{
	public class FakeProfile : Profile
	{
		public FakeProfile()
		{
			new FakeDto().Mapping(this);
		}
	}
}