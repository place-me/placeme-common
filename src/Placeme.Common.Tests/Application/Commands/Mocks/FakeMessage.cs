using System;
using AutoMapper;
using Placeme.Common.Application.Mappings;
using Placeme.Common.Tests.Application.Commands.Mocks;

namespace Placeme.Common.Tests.Generated.Base;
public partial class FakeMessage : IMapFrom<FakeEntity>
{
	public static Action<FakeMessage, FakeEntity, ResolutionContext> AfterMap { get; set; }

	public void Mapping(Profile profile)
	{
		profile.CreateMap<FakeEntity, FakeMessage>()
		.ForMember(r => r.Foobar, opt => opt.MapFrom(r => r.Foobar ?? string.Empty))
		.ForMember(r => r.Foobarer, opt => opt.MapFrom(r => r.Foobarer ?? string.Empty))
		.ForMember(r => r.Barfoo, opt => opt.MapFrom(r => r.Barfoo));

		profile.CreateMap<FakeMessage, FakeEntity>()
			.ForMember(r => r.Foobar, opt => opt.MapFrom(r => r.Foobar))
			.ForMember(r => r.Foobarer, opt =>
			{
				opt.MapFrom(r => r.Foobarer);
				opt.Condition(r => r.HasFoobarer);
			})
			.ForMember(r => r.Barfoo, opt => opt.MapFrom(r => r.Barfoo))
			.AfterMap((dto, entity, context) => AfterMap?.Invoke(dto, entity, context));
	}
}