using Placeme.Common.Domain;

namespace Placeme.Common.Tests.Application.Commands.Mocks
{
	public class FakeEntity : BaseEntity
	{
		public string Foobar { get; set; }
		public string Foobarer { get; set; }
		public int Barfoo { get; set; }
	}
}