﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NUnit.Framework;
using Placeme.Common.Tracing;

namespace Placeme.Common.Tests.Tracing
{
	[TestFixture]
	public class UberTraceIdPropagationMiddlewareFixture
	{
		[OneTimeSetUp]
		public void SetUpFixture()
		{
			Activity.DefaultIdFormat = ActivityIdFormat.W3C;
			Activity.ForceDefaultIdFormat = true;
		}

		[SetUp]
		public void Setup()
		{
			Activity.Current = new Activity("Some tracable activity").Start();
		}

		[TearDown]
		public void TearDown()
		{
			Activity.Current?.Stop();
		}

		private readonly RequestDelegate _mockRequestDelegate = _ => Task.CompletedTask;

		[Test]
		public void DoesntFail_IfNoActivityIsSet()
		{
			Activity.Current?.Stop();
			var context = new DefaultHttpContext();
			const string SpanId = "b7ad6b7169203331";
			const string TraceId = "696969696969";
			context.Request.Headers.Add("uber-trace-id", $"{TraceId}:{SpanId}:09890880:1");

			var middleware = new UberTraceIdPropagationMiddleware(_mockRequestDelegate);
			middleware.InvokeAsync(context).Wait();

			Assert.That(Activity.Current, Is.Null);
		}

		[Test]
		public void DoesNothing_NoHeaderPresent()
		{
			Activity.Current?.Stop();
			var context = new DefaultHttpContext();
			var middleware = new UberTraceIdPropagationMiddleware(_mockRequestDelegate);
			middleware.InvokeAsync(context).Wait();

			Assert.That(Activity.Current, Is.Null);
		}

		[Test]
		public void UeberTraceId_IsReadFromRequest()
		{
			var context = new DefaultHttpContext();
			const string SpanId = "b7ad6b7169203331";
			const string TraceId = "696969696969";
			context.Request.Headers.Add("uber-trace-id", $"{TraceId}:{SpanId}:09890880:1");

			var middleware = new UberTraceIdPropagationMiddleware(_mockRequestDelegate);
			middleware.InvokeAsync(context).Wait();

			Assert.That(Activity.Current, Is.Not.Null);
			Assert.That(Activity.Current.ParentSpanId.ToString(), Is.EqualTo(SpanId),
				"Failed to set ParentSpanId based on uber-trace-id");
			Assert.That(Activity.Current.TraceId.ToString(), Is.EqualTo(TraceId.PadLeft(32, '0')),
				"Failed to set correct TraceId on current Activity");
		}
	}
}