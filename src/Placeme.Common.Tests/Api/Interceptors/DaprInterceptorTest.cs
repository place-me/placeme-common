using System;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using NUnit.Framework;
using Placeme.Common.Api.Interceptors;
using static Grpc.Core.Interceptors.Interceptor;

namespace Placeme.Common.Tests.Api.Interceptors
{
	[TestFixture]
	public class DaprInterceptorTest
	{
		[OneTimeSetUp]
		public void Setup()
		{
		}

		[Test]
		public async Task DaprAppIdShouldBeInOutgoingRequest()
		{
			// var urlProvider = new PlacemeUrlProvider(_config);
			// Assert.That(urlProvider.LoginClient, Is.EqualTo(new Uri("https://login.placeme.ch")));
			var interceptor = new DaprInterceptor("beer-time-svc");
			var context = new ClientInterceptorContext<string, string>();
			AsyncUnaryCall<string> continuation(string _, ClientInterceptorContext<string, string> ctx)
			{
				var daprAppIdHeader = ctx.Options.Headers.Get("dapr-app-id");
				Assert.That(daprAppIdHeader, Is.Not.Null);
				Assert.That(daprAppIdHeader.Value, Is.EqualTo("beer-time-svc"));

				return new AsyncUnaryCall<string>(Task.FromResult(string.Empty), Task.FromResult(ctx.Options.Headers), () => Status.DefaultSuccess, () => ctx.Options.Headers, () => { });
			}
			await interceptor.AsyncUnaryCall(string.Empty, context, continuation);
		}
	}
}