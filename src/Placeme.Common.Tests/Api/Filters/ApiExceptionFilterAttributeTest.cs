using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using NUnit.Framework;
using Placeme.Common.Api.Filters;
using Placeme.Common.Application.Exceptions;

namespace Placeme.Common.Tests.Tracing
{
	[TestFixture]
	public class ApiExceptionFilterAttributeTest
	{
		[OneTimeSetUp]
		public void SetUpFixture()
		{
			Activity.DefaultIdFormat = ActivityIdFormat.W3C;
			Activity.ForceDefaultIdFormat = true;
		}

		[SetUp]
		public void Setup()
		{
			var context = new DefaultHttpContext();
			var actionContext = new ActionContext(context, new RouteData(), new ActionDescriptor());
			_exceptionContext = new ExceptionContext(actionContext, new List<IFilterMetadata>());
		}

		[TearDown]
		public void TearDown()
		{
			Activity.Current?.Stop();
		}

		private ExceptionContext _exceptionContext;

		[Test]
		public void UnhandledExceptionWithTraceId()
		{
			const string spanId = "b7ad6b7169203331";
			const string traceId = "696969696969";
			var activityTraceId = ActivityTraceId.CreateFromString(traceId.PadLeft(32, '0'));
			var activitySpanId = ActivitySpanId.CreateFromString(spanId.PadLeft(16, '0'));
			Activity.Current = new Activity("Some tracable activity").Start();
			Activity.Current.SetParentId(activityTraceId, activitySpanId);

			var exceptionFilter = new ApiExceptionFilterAttribute();
			_exceptionContext.Exception = new NullReferenceException();

			exceptionFilter.OnException(_exceptionContext);

			Assert.IsTrue(_exceptionContext.ExceptionHandled);
			Assert.IsInstanceOf<ObjectResult>(_exceptionContext.Result);
			var objectResult = _exceptionContext.Result as ObjectResult;
			Assert.IsInstanceOf<ProblemDetails>(objectResult.Value);
			var details = objectResult.Value as ProblemDetails;
			Assert.That(details.Status, Is.EqualTo(StatusCodes.Status500InternalServerError));
			Assert.That(details.Title, Is.EqualTo("An error occurred while processing your request."));
			Assert.That(details.Type, Is.EqualTo("https://tools.ietf.org/html/rfc7231#section-6.6.1"));
			Assert.That(details.Extensions.Count, Is.EqualTo(1));
			Assert.That(details.Extensions.Keys.First(), Is.EqualTo("traceid"));
			Assert.That(details.Extensions.Values.First(),
				Is.EqualTo($"{Activity.Current.TraceId}:{Activity.Current.SpanId}"));
			Assert.That(objectResult.StatusCode, Is.EqualTo(StatusCodes.Status500InternalServerError));
		}

		[Test]
		public void UnhandledExceptionWithoutTraceId()
		{
			var exceptionFilter = new ApiExceptionFilterAttribute();
			_exceptionContext.Exception = new NullReferenceException();

			exceptionFilter.OnException(_exceptionContext);

			Assert.IsTrue(_exceptionContext.ExceptionHandled);
			Assert.IsInstanceOf<ObjectResult>(_exceptionContext.Result);
			var objectResult = _exceptionContext.Result as ObjectResult;
			Assert.IsInstanceOf<ProblemDetails>(objectResult.Value);
			var details = objectResult.Value as ProblemDetails;
			Assert.That(details.Status, Is.EqualTo(StatusCodes.Status500InternalServerError));
			Assert.That(details.Title, Is.EqualTo("An error occurred while processing your request."));
			Assert.That(details.Type, Is.EqualTo("https://tools.ietf.org/html/rfc7231#section-6.6.1"));
			Assert.That(details.Extensions.Count, Is.EqualTo(0));
			Assert.That(objectResult.StatusCode, Is.EqualTo(StatusCodes.Status500InternalServerError));
		}

		[Test]
		public void ValidationException()
		{
			var exceptionFilter = new ApiExceptionFilterAttribute();
			_exceptionContext.Exception = new ValidationException();

			exceptionFilter.OnException(_exceptionContext);

			Assert.IsTrue(_exceptionContext.ExceptionHandled);
			Assert.IsInstanceOf<BadRequestObjectResult>(_exceptionContext.Result);
			var objectResult = _exceptionContext.Result as BadRequestObjectResult;
			Assert.IsInstanceOf<ValidationProblemDetails>(objectResult.Value);
			var details = objectResult.Value as ValidationProblemDetails;
			Assert.That(details.Type, Is.EqualTo("https://tools.ietf.org/html/rfc7231#section-6.5.1"));
		}

		[Test]
		public void NotFoundException()
		{
			var exceptionFilter = new ApiExceptionFilterAttribute();
			_exceptionContext.Exception = new NotFoundException("Some message");

			exceptionFilter.OnException(_exceptionContext);

			Assert.IsTrue(_exceptionContext.ExceptionHandled);
			Assert.IsInstanceOf<NotFoundObjectResult>(_exceptionContext.Result);
			var objectResult = _exceptionContext.Result as NotFoundObjectResult;
			Assert.IsInstanceOf<ProblemDetails>(objectResult.Value);
			var details = objectResult.Value as ProblemDetails;
			Assert.That(details.Type, Is.EqualTo("https://tools.ietf.org/html/rfc7231#section-6.5.4"));
			Assert.That(details.Title, Is.EqualTo("The specified resource was not found."));
			Assert.That(details.Detail, Is.EqualTo("Some message"));
		}

		[Test]
		public void UnauthorizedException()
		{
			var exceptionFilter = new ApiExceptionFilterAttribute();
			_exceptionContext.Exception = new UnauthorizedAccessException("Some message");

			exceptionFilter.OnException(_exceptionContext);

			Assert.IsTrue(_exceptionContext.ExceptionHandled);
			Assert.IsInstanceOf<ObjectResult>(_exceptionContext.Result);
			var objectResult = _exceptionContext.Result as ObjectResult;
			Assert.IsInstanceOf<ProblemDetails>(objectResult.Value);
			var details = objectResult.Value as ProblemDetails;
			Assert.That(details.Type, Is.EqualTo("https://tools.ietf.org/html/rfc7235#section-3.1"));
			Assert.That(details.Title, Is.EqualTo("Unauthorized"));
			Assert.That(details.Status, Is.EqualTo(StatusCodes.Status401Unauthorized));
			Assert.That(objectResult.StatusCode, Is.EqualTo(StatusCodes.Status401Unauthorized));
		}

		[Test]
		public void ForbiddenException()
		{
			var exceptionFilter = new ApiExceptionFilterAttribute();
			_exceptionContext.Exception = new ForbiddenAccessException();

			exceptionFilter.OnException(_exceptionContext);

			Assert.IsTrue(_exceptionContext.ExceptionHandled);
			Assert.IsInstanceOf<ObjectResult>(_exceptionContext.Result);
			var objectResult = _exceptionContext.Result as ObjectResult;
			Assert.IsInstanceOf<ProblemDetails>(objectResult.Value);
			var details = objectResult.Value as ProblemDetails;
			Assert.That(details.Type, Is.EqualTo("https://tools.ietf.org/html/rfc7231#section-6.5.3"));
			Assert.That(details.Title, Is.EqualTo("Forbidden"));
			Assert.That(details.Status, Is.EqualTo(StatusCodes.Status403Forbidden));
			Assert.That(objectResult.StatusCode, Is.EqualTo(StatusCodes.Status403Forbidden));
		}
	}
}