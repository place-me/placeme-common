using System.Collections.Generic;
using NUnit.Framework;
using Placeme.Common.Api.Parsers;
using Placeme.Common.Generated.Base;
using Placeme.Common.Tests.Application.Commands.Mocks;

namespace Placeme.Common.Tests.Api.Parsers
{
	[TestFixture]
	public class FilterToLinqParserTest
	{
		[SetUp]
		public void Setup()
		{
			_filterToLinqParser = new FilterToLinqParser();
		}

		private FilterToLinqParser _filterToLinqParser;

		[Test]
		public void CorrectlyParseEqualsFilter()
		{
			var filterDto = new Filter();
			filterDto.Left = "Foobar";
			filterDto.Right = "Test";
			filterDto.Operator = Operator.Equals;
			var filters = new List<Filter>();
			filters.Add(filterDto);
			var expression = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.And);
			Assert.That(expression.ToString(), Is.EqualTo("Param_0 => (Param_0.Foobar == \"Test\")"));
		}

		[Test]
		public void CorrectlyParseGreaterThanFilter()
		{
			var filterDto = new Filter();
			filterDto.Left = "Barfoo";
			filterDto.Right = "1";
			filterDto.Operator = Operator.GreaterThan;
			var filters = new List<Filter>();
			filters.Add(filterDto);
			var expression = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.And);
			Assert.That(expression.ToString(), Is.EqualTo("Param_0 => (Param_0.Barfoo > 1)"));
		}

		[Test]
		public void CorrectlyParseGreaterOrEqualThanFilter()
		{
			var filterDto = new Filter();
			filterDto.Left = "Barfoo";
			filterDto.Right = "1";
			filterDto.Operator = Operator.GreaterThanOrEqual;
			var filters = new List<Filter>();
			filters.Add(filterDto);
			var expression = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.And);
			Assert.That(expression.ToString(), Is.EqualTo("Param_0 => (Param_0.Barfoo >= 1)"));
		}

		[Test]
		public void CorrectlyParseNullFilter()
		{
			var filterDto = new Filter();
			filterDto.Left = "Foobar";
			filterDto.Right = "Foobarer";
			filterDto.CompareFields = true;
			filterDto.Operator = Operator.Equals;
			filterDto.AllowNull = true;
			var filters = new List<Filter>();
			filters.Add(filterDto);
			var expression = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.And);
			Assert.That(expression.ToString(), Is.EqualTo("Param_0 => ((Param_0.Foobar == null) OrElse (Param_0.Foobar == Param_0.Foobarer))"));
		}

		[Test]
		public void CorrectlyParseContainsFilter()
		{
			var filterDto = new Filter();
			filterDto.Left = "Foobar";
			filterDto.Right = "Test";
			filterDto.Operator = Operator.Contains;
			var filters = new List<Filter>();
			filters.Add(filterDto);
			var expression = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.And);
			Assert.That(expression.ToString(), Is.EqualTo("Param_0 => Param_0.Foobar.Contains(\"Test\")"));
		}

		[Test]
		public void CorrectlyParseNegatedFilter()
		{
			var filterDto = new Filter();
			filterDto.Left = "Foobar";
			filterDto.Right = "Test";
			filterDto.Negate = true;
			filterDto.Operator = Operator.Contains;
			var filters = new List<Filter>();
			filters.Add(filterDto);
			var expression = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.And);
			Assert.That(expression.ToString(), Is.EqualTo("Param_0 => Not(Param_0.Foobar.Contains(\"Test\"))"));
		}

		[Test]
		public void CorrectlyParseTypeFilter()
		{
			var filterDto = new Filter();
			filterDto.Left = "Foobar";
			filterDto.Right = "Foobarer";
			filterDto.CompareFields = true;
			filterDto.Operator = Operator.Equals;
			var filters = new List<Filter>();
			filters.Add(filterDto);
			var expression = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.And);
			Assert.That(expression.ToString(), Is.EqualTo("Param_0 => (Param_0.Foobar == Param_0.Foobarer)"));
		}

		[Test]
		public void CorrectlyParseMultipleFilters()
		{
			var filterDto = new Filter();
			filterDto.Left = "Barfoo";
			filterDto.Right = "1";
			filterDto.Operator = Operator.LessThan;
			var filterDto2 = new Filter();
			filterDto2.Left = "Barfoo";
			filterDto2.Right = "5";
			filterDto2.Operator = Operator.LessThanOrEqual;
			var filters = new List<Filter>();
			filters.Add(filterDto);
			filters.Add(filterDto2);
			var expressionAnd = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.And);
			Assert.That(expressionAnd.ToString(), Is.EqualTo("Param_0 => ((Param_0.Barfoo < 1) AndAlso (Param_0.Barfoo <= 5))"));
			var expressionOr = _filterToLinqParser.ConvertToLinqExpression<FakeEntity>(filters, LogicalOperator.Or);
			Assert.That(expressionOr.ToString(), Is.EqualTo("Param_0 => ((Param_0.Barfoo < 1) OrElse (Param_0.Barfoo <= 5))"));
		}
	}
}