using System;

namespace Placeme.Common.Identity;

public class CurrentUser : ICurrentUser
{
	public Guid UserprofileId { get; set; }

	public Guid TenantId { get; set; }
}