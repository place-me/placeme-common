﻿using System;

using Microsoft.AspNetCore.Http;

namespace Placeme.Common.Identity;

public class CurrentUserFromHttpContext : ICurrentUser
{
	private readonly IHttpContextAccessor _httpContextAccessor;

	public CurrentUserFromHttpContext(IHttpContextAccessor httpContextAccessor)
	{
		_httpContextAccessor = httpContextAccessor;
	}

	public Guid UserprofileId
	{
		get
		{
			if (_httpContextAccessor.HttpContext?.Items.ContainsKey("userprofileId") == true)
			{
				var profileAsString = _httpContextAccessor.HttpContext.Items["userprofileId"]?.ToString();
				return profileAsString != null ? Guid.Parse(profileAsString) : Guid.Empty;
			}

			return Guid.Empty;
		}
		set { }
	}

	public Guid TenantId
	{
		get
		{
			if (_httpContextAccessor.HttpContext?.Items.ContainsKey("tenantId") == true)
			{
				var tenantId = _httpContextAccessor.HttpContext.Items["tenantId"]?.ToString();
				return tenantId != null ? Guid.Parse(tenantId) : Guid.Empty;
			}

			return Guid.Empty;
		}
		set { }
	}
}