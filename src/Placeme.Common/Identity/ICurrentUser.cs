using System;

namespace Placeme.Common.Identity;

public interface ICurrentUser
{
	Guid UserprofileId { get; set; }
	Guid TenantId { get; set; }
}