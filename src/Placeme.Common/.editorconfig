# Editor configuration, see https://editorconfig.org
root = true

[*]
charset = utf-8-bom
indent_style = tab
end_of_line = lf
insert_final_newline = false
trim_trailing_whitespace = true
# (Please don't specify an indent_size here; that has too many unintended consequences.)

# Front-end dev
[*.{tsx,ts,js,jsx,json,css,scss,sass}]
indent_size = 2
max_line_length = 130
quote_type = single

[*.{cs,csx,vb,vbx}]
indent_size = 4

# XML project files
[*.{csproj,vbproj,vcxproj,vcxproj.filters,proj,projitems,shproj}]
indent_size = 2

# XML config files
[*.{props,targets,ruleset,config,nuspec,resx,vsixmanifest,vsct}]
indent_size = 2

[*.md]
max_line_length = off
trim_trailing_whitespace = false

[*.{json,cml}]
indent_size = 2

# Powershell files
[*.ps1]
indent_size = 2

# Shell script files
[*.sh]
end_of_line = lf
indent_size = 2

[*.{yml,yaml}]
indent_style = space
indent_size = 2
trim_trailing_whitespace = true
# Code Quality
# https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/quality-rules/
[*]
# https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/quality-rules/ca1848
# For high-performance logging scenarios, use the LoggerMessage pattern.
dotnet_diagnostic.CA1848.severity = silent

# Default analyzed API surface = 'all' (public APIs + non-public APIs)
dotnet_code_quality.api_surface = all

# CA1000: Do not declare static members on generic types
dotnet_diagnostic.CA1000.severity = none

# CA1002: Do not expose generic lists
dotnet_diagnostic.CA1002.severity = suggestion

# CA1024: Use properties where appropriate
dotnet_diagnostic.CA1024.severity = suggestion

# CA1033: Interface methods should be callable by child types
dotnet_diagnostic.CA1033.severity = suggestion

# CA1043: Use integral or string argument for indexers
# Restrict the analyzed API surface for certain analyzers to 'public' (public APIs only).
dotnet_code_quality.CA1043.api_surface = public

# CA1062: Validate arguments of public/protected methods
# An externally visible method dereferences one of its reference arguments without verifying whether that argument is null
# This predates the Nullable Reference Type language features.
dotnet_diagnostic.CA1062.severity = none

# CA1303: Do not pass literals as localized parameters
dotnet_diagnostic.CA1303.severity = none

# CA1307: Specify StringComparison for clarity
dotnet_diagnostic.CA1307.severity = suggestion

# CA1309: Use ordinal string comparison
dotnet_diagnostic.CA1309.severity = suggestion

# CA1707: Identifiers should not contain underscores
dotnet_code_quality.CA1707.api_surface = public

# CA1711: Identifiers should not have incorrect suffix
dotnet_diagnostic.CA1711.severity = suggestion
dotnet_code_quality.CA1711.allowed_suffixes = EventHandler|Permission

# CA1715: Identifiers should have correct prefix
# But exclude single letter type parameter names
dotnet_code_quality.CA1715.exclude_single_letter_type_parameters = true

# CA1716: Identifiers should not match keywords
dotnet_code_quality.CA1716.api_surface = private, internal

# CA1720: Identifiers should not contain type names
dotnet_code_quality.CA1720.api_surface = public

# CA1725: Parameter names should match base declaration
dotnet_diagnostic.CA1725.severity = suggestion

# CA1801: Unused parameters / IDE0060 / RCS1163
dotnet_diagnostic.CA1801.severity = suggestion

# CA2007: Do not directly await a Task - https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/quality-rules/ca2007
dotnet_diagnostic.CA2007.severity = none

# CA2227: Collection properties should be read only
dotnet_diagnostic.CA2227.severity = suggestion
# Compiler Messages: https://docs.microsoft.com/en-us/dotnet/csharp/misc/cs0183
[*]

# Default severity for analyzer diagnostics with category 'Style' (escalated to build warnings)
dotnet_analyzer_diagnostic.category-Style.severity = warning

# Default severity for all IDE code quality rules with category 'CodeQuality' (escalated to build warnings)
dotnet_analyzer_diagnostic.category-CodeQuality.severity = warning

# CS0168: Compiler Warning (level 3) - The variable 'var' is declared but never used
# https://docs.microsoft.com/en-us/dotnet/csharp/misc/cs0168
dotnet_diagnostic.CS0168.severity = error

# CS0219: Compiler Warning (level 3) - The variable 'variable' is assigned but its value is never used
# https://docs.microsoft.com/en-us/dotnet/csharp/misc/cs0219
dotnet_diagnostic.CS0219.severity = error

# CS1591: Compiler Warning (level 4) - Missing XML comment for publicly visible type or member
# https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-messages/cs1591
dotnet_diagnostic.CS1591.severity = none

# CS8019: Unnecessary using directive
dotnet_diagnostic.CS8019.severity = error

#==========================
# Nullable Reference types
#==========================
# CS8600: Converting null literal or possible null value to non-nullable type.
dotnet_diagnostic.CS8600.severity = error

# CS8602: Dereference of a possibly null reference.
dotnet_diagnostic.CS8602.severity = error

# CS8603: Possible null reference return.
dotnet_diagnostic.CS8603.severity = error

# CS8604: Possible null reference argument.
dotnet_diagnostic.CS8604.severity = error

# CS8605: Unboxing a possibly null value.
dotnet_diagnostic.CS8605.severity = error

# CS8606: Possible null reference assignment to iteration variable
dotnet_diagnostic.CS8606.severity = error

# CS8607: A possible null value may not be passed to a target marked with the [DisallowNull] attribute
dotnet_diagnostic.CS8607.severity = error

# CS8608: Nullability of reference types in type doesn't match overridden member.
dotnet_diagnostic.CS8608.severity = error

# CS8609: Nullability of reference types in return type doesn't match overridden member.
dotnet_diagnostic.CS8609.severity = error

# CS8610: Nullability of reference types in type of parameter doesn't match overridden member.
dotnet_diagnostic.CS8610.severity = error

# CS8611: Nullability of reference types in type of parameter doesn't match partial method declaration.
dotnet_diagnostic.CS8611.severity = error

# CS8612: Nullability of reference types in type doesn't match implicitly implemented member.
dotnet_diagnostic.CS8612.severity = error

# CS8613: Nullability of reference types in return type doesn't match implicitly implemented member.
dotnet_diagnostic.CS8613.severity = error

# CS8614: Nullability of reference types in type of parameter doesn't match implicitly implemented member.
dotnet_diagnostic.CS8614.severity = error

# CS8615: Nullability of reference types in type doesn't match implemented member.
dotnet_diagnostic.CS8615.severity = error

# CS8616: Nullability of reference types in return type doesn't match implemented member.
dotnet_diagnostic.CS8616.severity = error

# CS8617: Nullability of reference types in type of parameter doesn't match implemented member.
dotnet_diagnostic.CS8617.severity = error

# CS8618: Non-nullable property 'LastName' must contain a non-null value when exiting constructor. Consider declaring the property as nullable.
dotnet_diagnostic.CS8618.severity = error

# CS8619: Nullability of reference types in value doesn't match target type.
dotnet_diagnostic.CS8619.severity = error

# CS8620: Argument cannot be used for parameter due to differences in the nullability of reference types.
dotnet_diagnostic.CS8620.severity = error

# CS8621: Nullability of reference types in return type doesn't match the target delegate.
dotnet_diagnostic.CS8621.severity = error

# CS8622: Nullability of reference types in type of parameter doesn't match the target delegate.
dotnet_diagnostic.CS8622.severity = error

# CS8624: Argument cannot be used as an output for parameter due to differences in the nullability of reference types.
dotnet_diagnostic.CS8624.severity = error

# CS8625: Cannot convert null literal to non-nullable reference type.
dotnet_diagnostic.CS8625.severity = error

# CS8626: The 'as' operator may produce a null value for a type parameter.
dotnet_diagnostic.CS8626.severity = error

# CS8629: Nullable value type may be null.
dotnet_diagnostic.CS8629.severity = error

# CS8631: The type cannot be used as type parameter in the generic type or method. Nullability of type argument doesn't match constraint type.
dotnet_diagnostic.CS8631.severity = error

# CS8632: The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
dotnet_diagnostic.CS8632.severity = error

# CS8633: Nullability in constraints for type parameter doesn't match the constraints for type parameter in implicitly implemented interface method.
dotnet_diagnostic.CS8633.severity = error

# CS8634: The type cannot be used as type parameter in the generic type or method. Nullability of type argument doesn't match 'class' constraint.
dotnet_diagnostic.CS8634.severity = error

# CS8638: Conditional access may produce a null value for a type parameter.
dotnet_diagnostic.CS8638.severity = error

# CS8643: Nullability of reference types in explicit interface specifier doesn't match interface implemented by the type.
dotnet_diagnostic.CS8643.severity = error

# CS8644: Type does not implement interface member. Nullability of reference types in interface implemented by the base type doesn't match.
dotnet_diagnostic.CS8644.severity = error

# CS8645: Interface is already listed in the interface list with different nullability of reference types.
dotnet_diagnostic.CS8645.severity = error

# CS8653: A default expression introduces a null value for a type parameter.
dotnet_diagnostic.CS8653.severity = error

# CS8654: A null literal introduces a null value for a type parameter.
dotnet_diagnostic.CS8654.severity = error

# CS8655: The switch expression does not handle some null inputs.
dotnet_diagnostic.CS8655.severity = error

# CS8667: Partial method declarations have inconsistent nullability in constraints for type parameter
dotnet_diagnostic.CS8667.severity = error

# CS8714: The type cannot be used as type parameter in the generic type or method. Nullability of type argument doesn't match 'notnull' constraint.
dotnet_diagnostic.CS8714.severity = error

[obj/**]
# CS8019: Unnecessary using directive
dotnet_diagnostic.CS8019.severity = none
# .NET Formatting rules
# https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules

# The formatting rules in this section apply to both C# and Visual Basic.
[*.{cs,vb}]
# IDE0055: Fix formatting - All formatting options have rule ID IDE0055 and title Fix formatting.
#          Set the severity of a formatting violation in an EditorConfig file using the following configuration line.
dotnet_diagnostic.IDE0055.severity = error

# Sort using and Import directives with System.* appearing first
dotnet_separate_import_directive_groups = true
dotnet_sort_system_directives_first = true

# https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules#project-items
dotnet_style_namespace_match_folder = true
dotnet_diagnostic.IDE0130.severity = error

# Avoid "this." and "Me." if not necessary
# https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/ide0003-ide0009
dotnet_style_qualification_for_field = false:refactoring
dotnet_style_qualification_for_property = false:refactoring
dotnet_style_qualification_for_method = false:refactoring
dotnet_style_qualification_for_event = false:refactoring

# The formatting rules in this section apply only to C# code.
[*.cs]
# Newline settings: https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules#new-line-options
csharp_new_line_before_open_brace = all
csharp_new_line_before_else = true
csharp_new_line_before_catch = true
csharp_new_line_before_finally = true
csharp_new_line_before_members_in_object_initializers = true
csharp_new_line_before_members_in_anonymous_types = true
csharp_new_line_between_query_expression_clauses = true

# Indentation preferences: https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules#indentation-options
csharp_indent_block_contents = true
csharp_indent_braces = false
csharp_indent_case_contents = true
csharp_indent_case_contents_when_block = false
csharp_indent_switch_labels = true
csharp_indent_labels = flush_left

# Space preferences: https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules#spacing-options
csharp_space_after_cast = false
csharp_space_after_keywords_in_control_flow_statements = true
csharp_space_after_colon_in_inheritance_clause = true
csharp_space_after_comma = true
csharp_space_after_dot = false
csharp_space_after_semicolon_in_for_statement = true
csharp_space_around_binary_operators = before_and_after
csharp_space_around_declaration_statements = do_not_ignore
csharp_space_before_colon_in_inheritance_clause = true
csharp_space_before_comma = false
csharp_space_before_dot = false
csharp_space_before_open_square_brackets = false
csharp_space_before_semicolon_in_for_statement = false
csharp_space_between_empty_square_brackets = false
csharp_space_between_method_call_empty_parameter_list_parentheses = false
csharp_space_between_method_call_name_and_opening_parenthesis = false
csharp_space_between_method_call_parameter_list_parentheses = false
csharp_space_between_method_declaration_empty_parameter_list_parentheses = false
csharp_space_between_method_declaration_name_and_open_parenthesis = false
csharp_space_between_method_declaration_parameter_list_parentheses = false
csharp_space_between_parentheses = false
csharp_space_between_square_brackets = false

# Blocks are allowed: https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules#wrap-options
csharp_preserve_single_line_blocks = true
csharp_preserve_single_line_statements = true

csharp_prefer_simple_using_statement = true:warning

# using directives: https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules#using-directive-options
csharp_using_directive_placement = outside_namespace

# Namespace options: https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/formatting-rules#namespace-options
csharp_style_namespace_declarations = file_scoped
# The style rules in this section apply to both C# and Visual Basic.
[*.{cs,vb}]
# Use language keywords instead of framework type names for type references
dotnet_style_predefined_type_for_locals_parameters_members = true:suggestion
dotnet_style_predefined_type_for_member_access = true:suggestion

# Suggest more modern language features when available
dotnet_style_object_initializer = true:suggestion
dotnet_style_collection_initializer = true:suggestion
dotnet_style_coalesce_expression = true:suggestion
dotnet_style_null_propagation = true:suggestion
dotnet_style_explicit_tuple_names = true:suggestion

# Non-private static fields are PascalCase
dotnet_naming_rule.non_private_static_fields_should_be_pascal_case.severity = suggestion
dotnet_naming_rule.non_private_static_fields_should_be_pascal_case.symbols = non_private_static_fields
dotnet_naming_rule.non_private_static_fields_should_be_pascal_case.style = non_private_static_field_style

dotnet_naming_symbols.non_private_static_fields.applicable_kinds = field
dotnet_naming_symbols.non_private_static_fields.applicable_accessibilities = public, protected, internal, protected_internal, private_protected
dotnet_naming_symbols.non_private_static_fields.required_modifiers = static

dotnet_naming_style.non_private_static_field_style.capitalization = pascal_case

# Non-private readonly fields are PascalCase
dotnet_naming_rule.non_private_readonly_fields_should_be_pascal_case.severity = suggestion
dotnet_naming_rule.non_private_readonly_fields_should_be_pascal_case.symbols = non_private_readonly_fields
dotnet_naming_rule.non_private_readonly_fields_should_be_pascal_case.style = non_private_readonly_field_style

dotnet_naming_symbols.non_private_readonly_fields.applicable_kinds = field
dotnet_naming_symbols.non_private_readonly_fields.applicable_accessibilities = public, protected, internal, protected_internal, private_protected
dotnet_naming_symbols.non_private_readonly_fields.required_modifiers = readonly

dotnet_naming_style.non_private_readonly_field_style.capitalization = pascal_case

# Constants are PascalCase
dotnet_naming_rule.constants_should_be_pascal_case.severity = suggestion
dotnet_naming_rule.constants_should_be_pascal_case.symbols = constants
dotnet_naming_rule.constants_should_be_pascal_case.style = constant_style

dotnet_naming_symbols.constants.applicable_kinds = field, local
dotnet_naming_symbols.constants.required_modifiers = const

dotnet_naming_style.constant_style.capitalization = pascal_case

# Static fields are camelCase and start with s_
dotnet_naming_rule.static_fields_should_be_camel_case.severity = suggestion
dotnet_naming_rule.static_fields_should_be_camel_case.symbols = static_fields
dotnet_naming_rule.static_fields_should_be_camel_case.style = static_field_style

dotnet_naming_symbols.static_fields.applicable_kinds = field
dotnet_naming_symbols.static_fields.required_modifiers = static

dotnet_naming_style.static_field_style.capitalization = pascal_case

# Instance fields are camelCase and start with _
dotnet_naming_rule.instance_fields_should_be_camel_case.severity = suggestion
dotnet_naming_rule.instance_fields_should_be_camel_case.symbols = instance_fields
dotnet_naming_rule.instance_fields_should_be_camel_case.style = instance_field_style

dotnet_naming_symbols.instance_fields.applicable_kinds = field

dotnet_naming_style.instance_field_style.capitalization = camel_case
dotnet_naming_style.instance_field_style.required_prefix = _

# Locals and parameters are camelCase
dotnet_naming_rule.locals_should_be_camel_case.severity = suggestion
dotnet_naming_rule.locals_should_be_camel_case.symbols = locals_and_parameters
dotnet_naming_rule.locals_should_be_camel_case.style = camel_case_style

dotnet_naming_symbols.locals_and_parameters.applicable_kinds = parameter, local

dotnet_naming_style.camel_case_style.capitalization = camel_case

# Local functions are PascalCase
dotnet_naming_rule.local_functions_should_be_pascal_case.severity = suggestion
dotnet_naming_rule.local_functions_should_be_pascal_case.symbols = local_functions
dotnet_naming_rule.local_functions_should_be_pascal_case.style = local_function_style

dotnet_naming_symbols.local_functions.applicable_kinds = local_function

dotnet_naming_style.local_function_style.capitalization = pascal_case

# By default, name items with PascalCase
dotnet_naming_rule.members_should_be_pascal_case.severity = suggestion
dotnet_naming_rule.members_should_be_pascal_case.symbols = all_members
dotnet_naming_rule.members_should_be_pascal_case.style = pascal_case_style

dotnet_naming_symbols.all_members.applicable_kinds = *

dotnet_naming_style.pascal_case_style.capitalization = pascal_case

# error RS2008: Enable analyzer release tracking for the analyzer project containing rule '{0}'
dotnet_diagnostic.RS2008.severity = none

# IDE0073: File header
# dotnet_diagnostic.IDE0073.severity = warning
# file_header_template = Licensed to the .NET Foundation under one or more agreements./nThe .NET Foundation licenses this file to you under the MIT license./nSee the LICENSE file in the project root for more information.

# IDE0005: Remove unnecessary import
dotnet_diagnostic.IDE0005.severity = error

# IDE0007: Use 'var' instead of explicit type
dotnet_diagnostic.IDE0007.severity = suggestion

# IDE0008: Use explicit type instead of 'var'
dotnet_diagnostic.IDE0008.severity = none

# IDE0010: Add missing cases in switch statement
dotnet_diagnostic.IDE0010.severity = none

# IDE0021: Use expression body for constructors
dotnet_diagnostic.IDE0021.severity = none

# IDE0035: Remove unreachable code
dotnet_diagnostic.IDE0035.severity = error

# IDE0036: Order modifiers
dotnet_diagnostic.IDE0036.severity = warning

# IDE0043: Format string contains invalid placeholder
dotnet_diagnostic.IDE0043.severity = warning

# IDE0044: Make field readonly
dotnet_diagnostic.IDE0044.severity = warning

# IDE0046: Use conditional expression for return 
dotnet_diagnostic.IDE0046.severity = none

# IDE0050: Convert anonymous type to tuple
dotnet_diagnostic.IDE0050.severity = suggestion

# IDE0055: Fix formatting
dotnet_diagnostic.IDE0055.severity = error

# IDE0058: Remove unnecessary expression value / csharp_style_unused_value_expression_statement_preference
dotnet_diagnostic.IDE0058.severity = none

# IDE0063: Use simple 'using' statement / csharp_prefer_simple_using_statement
dotnet_diagnostic.IDE0063.severity = suggestion

# IDE0066: Convert switch statement to expression
dotnet_diagnostic.IDE0066.severity = suggestion

# IDE0071: Simplify interpolation / dotnet_style_prefer_simplified_interpolation
dotnet_diagnostic.IDE0071.severity = suggestion

# IDE1006: Naming rule violation
dotnet_diagnostic.IDE1006.severity = error

# RS0016: Only enable if API files are present
dotnet_public_api_analyzer.require_api_files = true

#[src/CodeStyle/**.{cs,vb}]
## warning RS0005: Do not use generic CodeAction.Create to create CodeAction
#dotnet_diagnostic.RS0005.severity = none

# The style rules in this section apply only to C# code.
[*.cs]
# Prefer "var" everywhere
csharp_style_var_for_built_in_types = true:suggestion
csharp_style_var_when_type_is_apparent = true:suggestion
csharp_style_var_elsewhere = true:suggestion

# Prefer method-like constructs to have a block body
csharp_style_expression_bodied_methods = false:none
csharp_style_expression_bodied_constructors = false:none
csharp_style_expression_bodied_operators = false:none

# Prefer property-like constructs to have an expression-body
csharp_style_expression_bodied_properties = true:none
csharp_style_expression_bodied_indexers = true:none
csharp_style_expression_bodied_accessors = true:none

# Suggest more modern language features when available
csharp_style_pattern_matching_over_is_with_cast_check = true:suggestion
csharp_style_pattern_matching_over_as_with_null_check = true:suggestion
csharp_style_conditional_delegate_call = true:suggestion
csharp_style_unused_value_assignment_preference = discard_variable:warning
csharp_style_deconstructed_variable_declaration = true:suggestion
csharp_style_inlined_variable_declaration = true:suggestion
csharp_style_pattern_local_over_anonymous_function = true:suggestion
csharp_style_prefer_index_operator = true:suggestion
csharp_style_prefer_range_operator = true:suggestion
csharp_style_throw_expression = true:suggestion
csharp_style_unused_value_expression_statement_preference = discard_variable:silent

# Modifier preferences
csharp_prefer_simple_default_expression = true:suggestion
csharp_prefer_static_local_function = true:suggestion
csharp_preferred_modifier_order = public,private,protected,internal,static,extern,new,virtual,abstract,sealed,override,readonly,unsafe,volatile,async:suggestion

[src/{Analyzers,CodeStyle,Features,Workspaces,EditorFeatures, VisualStudio}/**/*.{cs,vb}]

# IDE0011: Add braces
csharp_prefer_braces = when_multiline:warning
# NOTE: We need the below severity entry for Add Braces due to https://github.com/dotnet/roslyn/issues/44201
dotnet_diagnostic.IDE0011.severity = warning

# IDE0040: Add accessibility modifiers
dotnet_diagnostic.IDE0040.severity = warning

# CONSIDER: Are IDE0051 and IDE0052 too noisy to be warnings for IDE editing scenarios? Should they be made build-only warnings?
# IDE0051: Remove unused private member
dotnet_diagnostic.IDE0051.severity = warning

# IDE0052: Remove unread private member
dotnet_diagnostic.IDE0052.severity = warning

# IDE0059: Unnecessary assignment to a value
dotnet_diagnostic.IDE0059.severity = warning

# IDE0060: Remove unused parameter
dotnet_diagnostic.IDE0060.severity = warning

# CA1822: Make member static
dotnet_diagnostic.CA1822.severity = warning

# Prefer "var" everywhere
csharp_style_var_for_built_in_types = true:warning
csharp_style_var_when_type_is_apparent = true:warning
csharp_style_var_elsewhere = true:warning

[src/{VisualStudio}/**/*.{cs,vb}]
# CA1822: Make member static
# Not enforced as a build 'warning' for 'VisualStudio' layer due to large number of false positives from https://github.com/dotnet/roslyn-analyzers/issues/3857 and https://github.com/dotnet/roslyn-analyzers/issues/3858
# Additionally, there is a risk of accidentally breaking an internal API that partners rely on though IVT.
dotnet_diagnostic.CA1822.severity = suggestion
# Roslynator Analysers
# https://github.com/JosefPihrt/Roslynator/blob/master/src/Analyzers/README.md
[*]

# Formatting analyzers (RCS0xxx) - http://pihrt.net/Roslynator/Analyzers?Query=RCS0

# Common analyzers (RCS1xxx) - http://pihrt.net/Roslynator/Analyzers?Query=RCS1
dotnet_diagnostic.RCS1007.severity = error

# Covered by IDE0007
# RCS1010: Use 'var' instead of explicit type
dotnet_diagnostic.RCS1010.severity = none

dotnet_diagnostic.RCS1013.severity = error
dotnet_diagnostic.RCS1014.severity = error
dotnet_diagnostic.RCS1015.severity = error
dotnet_diagnostic.RCS1019.severity = error
dotnet_diagnostic.RCS1020.severity = error
dotnet_diagnostic.RCS1021.severity = error
dotnet_diagnostic.RCS1024.severity = error
dotnet_diagnostic.RCS1027.severity = error
dotnet_diagnostic.RCS1028.severity = error
dotnet_diagnostic.RCS1032.severity = error
dotnet_diagnostic.RCS1033.severity = error
dotnet_diagnostic.RCS1035.severity = error
dotnet_diagnostic.RCS1036.severity = error
dotnet_diagnostic.RCS1037.severity = error
dotnet_diagnostic.RCS1038.severity = error
dotnet_diagnostic.RCS1039.severity = error
dotnet_diagnostic.RCS1040.severity = error
dotnet_diagnostic.RCS1041.severity = error
dotnet_diagnostic.RCS1044.severity = error
dotnet_diagnostic.RCS1045.severity = error
dotnet_diagnostic.RCS1047.severity = error
dotnet_diagnostic.RCS1048.severity = error
dotnet_diagnostic.RCS1049.severity = error
dotnet_diagnostic.RCS1052.severity = error
dotnet_diagnostic.RCS1055.severity = error
dotnet_diagnostic.RCS1057.severity = error
dotnet_diagnostic.RCS1058.severity = error
dotnet_diagnostic.RCS1059.severity = error
dotnet_diagnostic.RCS1061.severity = error
dotnet_diagnostic.RCS1062.severity = error
dotnet_diagnostic.RCS1063.severity = error
dotnet_diagnostic.RCS1064.severity = error
dotnet_diagnostic.RCS1066.severity = error
dotnet_diagnostic.RCS1068.severity = error
dotnet_diagnostic.RCS1069.severity = error
dotnet_diagnostic.RCS1070.severity = error
dotnet_diagnostic.RCS1071.severity = error
dotnet_diagnostic.RCS1072.severity = error
dotnet_diagnostic.RCS1073.severity = error
dotnet_diagnostic.RCS1074.severity = error
dotnet_diagnostic.RCS1075.severity = error
dotnet_diagnostic.RCS1076.severity = error
dotnet_diagnostic.RCS1077.severity = error
dotnet_diagnostic.RCS1079.severity = error
dotnet_diagnostic.RCS1081.severity = error
dotnet_diagnostic.RCS1084.severity = error
dotnet_diagnostic.RCS1085.severity = error
dotnet_diagnostic.RCS1088.severity = error
# RCS1090: Add call to 'ConfigureAwait' - https://github.com/JosefPihrt/Roslynator/blob/master/docs/analyzers/RCS1090.md
dotnet_diagnostic.RCS1090.severity = none
dotnet_diagnostic.RCS1091.severity = error
dotnet_diagnostic.RCS1093.severity = error
dotnet_diagnostic.RCS1094.severity = error
dotnet_diagnostic.RCS1097.severity = error
dotnet_diagnostic.RCS1098.severity = error
dotnet_diagnostic.RCS1099.severity = error
dotnet_diagnostic.RCS1103.severity = error
dotnet_diagnostic.RCS1104.severity = error
dotnet_diagnostic.RCS1105.severity = error
dotnet_diagnostic.RCS1106.severity = error
dotnet_diagnostic.RCS1107.severity = error
dotnet_diagnostic.RCS1110.severity = error
dotnet_diagnostic.RCS1112.severity = error
dotnet_diagnostic.RCS1113.severity = error
dotnet_diagnostic.RCS1114.severity = error
dotnet_diagnostic.RCS1123.severity = error
dotnet_diagnostic.RCS1126.severity = error
dotnet_diagnostic.RCS1127.severity = error
dotnet_diagnostic.RCS1128.severity = error
dotnet_diagnostic.RCS1132.severity = error

# RCS1138: Add summary to documentation comment
dotnet_diagnostic.RCS1138.severity = none

# RCS1139: Add summary element to documentation comment
dotnet_diagnostic.RCS1139.severity = none

dotnet_diagnostic.RCS1146.severity = error
dotnet_diagnostic.RCS1151.severity = error
dotnet_diagnostic.RCS1153.severity = error
dotnet_diagnostic.RCS1154.severity = error
dotnet_diagnostic.RCS1155.severity = error
dotnet_diagnostic.RCS1160.severity = error
dotnet_diagnostic.RCS1162.severity = error

# Covered by CA1801: Unused parameters
dotnet_diagnostic.RCS1163.severity = none

dotnet_diagnostic.RCS1164.severity = error
dotnet_diagnostic.RCS1166.severity = error

# RCS1168: Parameter name differs from base name
dotnet_diagnostic.RCS1168.severity = suggestion

dotnet_diagnostic.RCS1171.severity = error
dotnet_diagnostic.RCS1172.severity = error
dotnet_diagnostic.RCS1173.severity = error
dotnet_diagnostic.RCS1175.severity = error
dotnet_diagnostic.RCS1179.severity = error
dotnet_diagnostic.RCS1182.severity = error
dotnet_diagnostic.RCS1186.severity = error
dotnet_diagnostic.RCS1187.severity = error
dotnet_diagnostic.RCS1188.severity = error
dotnet_diagnostic.RCS1190.severity = error
dotnet_diagnostic.RCS1191.severity = error
dotnet_diagnostic.RCS1192.severity = error
dotnet_diagnostic.RCS1193.severity = error
dotnet_diagnostic.RCS1194.severity = error
dotnet_diagnostic.RCS1195.severity = error
dotnet_diagnostic.RCS1196.severity = error
dotnet_diagnostic.RCS1197.severity = error
dotnet_diagnostic.RCS1198.severity = none
dotnet_diagnostic.RCS1199.severity = error
dotnet_diagnostic.RCS1201.severity = error
dotnet_diagnostic.RCS1202.severity = error
dotnet_diagnostic.RCS1203.severity = error
dotnet_diagnostic.RCS1204.severity = error
dotnet_diagnostic.RCS1205.severity = error
dotnet_diagnostic.RCS1206.severity = error
dotnet_diagnostic.RCS1208.severity = error
dotnet_diagnostic.RCS1209.severity = error
dotnet_diagnostic.RCS1210.severity = error
dotnet_diagnostic.RCS1211.severity = error
dotnet_diagnostic.RCS1212.severity = error
dotnet_diagnostic.RCS1213.severity = error
dotnet_diagnostic.RCS1214.severity = error
dotnet_diagnostic.RCS1215.severity = error
dotnet_diagnostic.RCS1216.severity = error
dotnet_diagnostic.RCS1218.severity = error
dotnet_diagnostic.RCS1224.severity = error
dotnet_diagnostic.RCS1225.severity = error
dotnet_diagnostic.RCS1229.severity = error
dotnet_diagnostic.RCS1230.severity = error
dotnet_diagnostic.RCS1234.severity = error
dotnet_diagnostic.RCS1235.severity = error
dotnet_diagnostic.RCS1236.severity = error
dotnet_diagnostic.RCS1239.severity = error
dotnet_diagnostic.RCS1240.severity = error
dotnet_diagnostic.RCS1243.severity = error
dotnet_diagnostic.RCS1244.severity = error

[obj/**]
dotnet_diagnostic.RCS1093.severity = none
# Roslynator Fixes
# https://github.com/JosefPihrt/Roslynator/blob/master/src/CodeFixes/README.md
[*]

# Enable/disable all compiler diagnostic fixes
roslynator_compiler_diagnostic_fixes.enabled = true

# Enable/disable specific compiler diagnostic fix
# roslynator_compiler_diagnostic_fix.<COMPILER_DIAGNOSTIC_ID>.enabled = true|false
# Roslynator Refactorings
# https://github.com/JosefPihrt/Roslynator/blob/master/src/Refactorings/README.md
[*]

# Enable/disable all refactorings
roslynator_refactorings.enabled = true

# Enable/disable specific refactoring
# roslynator_refactoring.<REFACTORING_NAME>.enabled = true|false
