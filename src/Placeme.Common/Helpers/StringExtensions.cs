
using System;

namespace Placeme.Common.Helpers;

public static class StringExtensions
{
	/// <summary>
	/// Parses the given string into a Guid. If not successfull an empty Guid is returned.
	/// </summary>
	public static Guid AsGuid(this string? value)
	{
		if (Guid.TryParse(value ?? string.Empty, out var parsedValue))
		{
			return parsedValue;
		}
		return Guid.Empty;
	}
}