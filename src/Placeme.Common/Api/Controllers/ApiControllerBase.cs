﻿using MediatR;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

using Placeme.Common.Api.Filters;

namespace Placeme.Common.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
[ApiExceptionFilter]
public abstract class ApiControllerBase : ControllerBase
{
	protected ISender Mediator => HttpContext.RequestServices.GetService<ISender>()!;
}