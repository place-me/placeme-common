using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Placeme.Common.Api.Middlewares;

public class CurrentUserFromConfigMiddleware
{
	private readonly IConfiguration _configuration;
	private readonly RequestDelegate _next;

	public CurrentUserFromConfigMiddleware(RequestDelegate next, IConfiguration configuration)
	{
		_next = next;
		_configuration = configuration;
	}

	public async Task Invoke(HttpContext context)
	{
		context.Items["userprofileId"] = _configuration.GetValue("USERPROFILE_UUID", Guid.Empty);
		context.Items["tenantId"] = _configuration.GetValue("TENANT_UUID", Guid.Empty);

		await _next.Invoke(context);
	}
}