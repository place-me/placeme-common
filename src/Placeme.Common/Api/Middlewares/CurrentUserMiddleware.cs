using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

namespace Placeme.Common.Api.Middlewares;

public class CurrentUserMiddleware
{
	private readonly RequestDelegate _next;

	public CurrentUserMiddleware(RequestDelegate next)
	{
		_next = next;
	}

	public async Task Invoke(HttpContext context)
	{
		var userprofileIdForRequest = Guid.Empty;
		var tenantIdForRequest = Guid.Empty;

		if (context.Request.Headers.ContainsKey("Placeme-Context"))
		{
			var vals = context.Request.Headers.GetCommaSeparatedValues("Placeme-Context");
			if (vals.Length > 0 && Guid.TryParse(vals[0], out var userprofileId))
			{
				userprofileIdForRequest = userprofileId;
			}

			if (vals.Length > 1 && Guid.TryParse(vals[1], out var tenantId))
			{
				tenantIdForRequest = tenantId;
			}
		}

		context.Items["userprofileId"] = userprofileIdForRequest;
		context.Items["tenantId"] = tenantIdForRequest;

		await _next.Invoke(context);
	}
}