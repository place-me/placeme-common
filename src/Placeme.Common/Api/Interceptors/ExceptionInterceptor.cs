using System;
using System.Threading.Tasks;

using Grpc.Core;
using Grpc.Core.Interceptors;

namespace Placeme.Common.Api.Interceptors;

// https://the-worst.dev/how-to-handle-grpc-errors-in-net-core/
public class ExceptionInterceptor : Interceptor
{
	public sealed override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request,
		ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
	{
		try
		{
			return await continuation(request, context);
		}
		catch (NotSupportedException e)
		{
			throw new RpcException(new Status(StatusCode.InvalidArgument, e.ToString()));
		}
		catch (ArgumentException e)
		{
			throw new RpcException(new Status(StatusCode.InvalidArgument, e.ToString()));
		}
		catch (Exception e)
		{
			throw new RpcException(new Status(StatusCode.Internal, e.ToString()));
		}
	}
}