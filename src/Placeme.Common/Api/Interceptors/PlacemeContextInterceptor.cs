using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Grpc.Core;
using Grpc.Core.Interceptors;

using Placeme.Common.Identity;

namespace Placeme.Common.Api.Interceptors;

public class PlacemeContextInterceptor : Interceptor
{
	private readonly ICurrentUser _currentUser;

	public PlacemeContextInterceptor(ICurrentUser currentUser)
	{
		_currentUser = currentUser;
	}

	public override AsyncUnaryCall<TResponse> AsyncUnaryCall<TRequest, TResponse>(TRequest request,
		ClientInterceptorContext<TRequest, TResponse> context,
		AsyncUnaryCallContinuation<TRequest, TResponse> continuation)
	{
		var contextInfos = new List<string>();
		if (_currentUser.UserprofileId != Guid.Empty)
		{
			contextInfos.Add(_currentUser.UserprofileId.ToString());
		}

		if (_currentUser.TenantId != Guid.Empty)
		{
			contextInfos.Add(_currentUser.TenantId.ToString());
		}

		var additionalMetadata = new Metadata { { "placeme-context", string.Join(',', contextInfos) } };

		// Copy existing headers if present
		if (context.Options.Headers != null)
		{
			foreach (var header in context.Options.Headers)
			{
				additionalMetadata.Add(header);
			}
		}

		var newOptions = context.Options.WithHeaders(additionalMetadata);
		var newContext = new ClientInterceptorContext<TRequest, TResponse>(context.Method, context.Host, newOptions);
		return base.AsyncUnaryCall(request, newContext, continuation);
	}

	public sealed override Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request,
		ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
	{
		var contextHeader = context.RequestHeaders.Get("placeme-context");
		if (contextHeader == null)
		{
			return base.UnaryServerHandler(request, context, continuation);
		}

		var userprofileIdForRequest = Guid.Empty;
		var tenantIdForRequest = Guid.Empty;

		var vals = contextHeader.Value.Split(',');
		if (vals.Length > 0 && Guid.TryParse(vals[0], out var userprofileId))
		{
			userprofileIdForRequest = userprofileId;
		}

		if (vals.Length > 1 && Guid.TryParse(vals[1], out var tenantId))
		{
			tenantIdForRequest = tenantId;
		}

		_currentUser.UserprofileId = userprofileIdForRequest;
		_currentUser.TenantId = tenantIdForRequest;

		return base.UnaryServerHandler(request, context, continuation);
	}
}