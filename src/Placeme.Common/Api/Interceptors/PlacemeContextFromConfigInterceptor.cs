using System;
using System.Threading.Tasks;

using Grpc.Core;
using Grpc.Core.Interceptors;

using Microsoft.Extensions.Configuration;

using Placeme.Common.Identity;

namespace Placeme.Common.Api.Interceptors;

public class PlacemeContextFromConfigInterceptor : Interceptor
{
	private readonly IConfiguration _configuration;
	private readonly ICurrentUser _currentUser;

	public PlacemeContextFromConfigInterceptor(ICurrentUser currentUser, IConfiguration configuration)
	{
		_currentUser = currentUser;
		_configuration = configuration;
	}

	public sealed override Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request,
		ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
	{
		_currentUser.UserprofileId = _configuration.GetValue("USERPROFILE_UUID", Guid.Empty);
		_currentUser.TenantId = _configuration.GetValue("TENANT_UUID", Guid.Empty);

		return base.UnaryServerHandler(request, context, continuation);
	}
}