using Grpc.Core;
using Grpc.Core.Interceptors;

namespace Placeme.Common.Api.Interceptors;

public class DaprInterceptor : Interceptor
{
	private readonly string _appId;

	public DaprInterceptor(string appId)
	{
		_appId = appId;
	}

	public override AsyncUnaryCall<TResponse> AsyncUnaryCall<TRequest, TResponse>(TRequest request,
		ClientInterceptorContext<TRequest, TResponse> context,
		AsyncUnaryCallContinuation<TRequest, TResponse> continuation)
	{
		var daprMetadata = new Metadata { { "dapr-app-id", _appId } };

		// Copy existing headers if present
		if (context.Options.Headers != null)
		{
			foreach (var header in context.Options.Headers)
			{
				daprMetadata.Add(header);
			}
		}

		var newOptions = context.Options.WithHeaders(daprMetadata);
		var newContext = new ClientInterceptorContext<TRequest, TResponse>(context.Method, context.Host, newOptions);
		return base.AsyncUnaryCall(request, newContext, continuation);
	}
}