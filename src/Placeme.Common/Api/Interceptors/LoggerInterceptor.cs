using System;
using System.Threading.Tasks;

using Grpc.Core;
using Grpc.Core.Interceptors;

using Serilog;

namespace Placeme.Common.Api.Interceptors;

public class LoggerInterceptor : Interceptor
{
	private readonly ILogger _logger;

	public LoggerInterceptor(ILogger logger)
	{
		_logger = logger;
	}

	public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
	{
		var httpContext = context.GetHttpContext();
		_logger.Debug("Starting grpc call.", httpContext.Request);
		TResponse response;
		try
		{
			response = await continuation(request, context);
		}
		catch (Exception e)
		{
			_logger.Error(e, "An error occured during grpc call", context);
			throw new RpcException(Status.DefaultCancelled, e.Message);
		}
		_logger.Debug("Ending grpc call.", httpContext.Request);
		return response;
	}
}