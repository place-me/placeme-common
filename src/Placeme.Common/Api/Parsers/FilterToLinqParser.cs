using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;

using Placeme.Common.Application.Interfaces;
using Placeme.Common.Generated.Base;

namespace Placeme.Common.Api.Parsers;

public class FilterToLinqParser : IFilterParser
{
	public Expression<Func<T, bool>> ConvertToLinqExpression<T>(IEnumerable<Filter> filters, LogicalOperator logicalOperator)
	{
		return (Expression<Func<T, bool>>)DynamicExpressionParser.ParseLambda(typeof(T), typeof(bool), ConvertFiltersToExpression(filters, logicalOperator));
	}

	private static string ConvertFiltersToExpression(IEnumerable<Filter> filters, LogicalOperator logicalOperator)
	{
		var fullExpression = string.Empty;
		foreach (var filter in filters)
		{
			if (!string.IsNullOrWhiteSpace(fullExpression))
			{
				var op = logicalOperator switch
				{
					LogicalOperator.And => "&&",
					LogicalOperator.Or => "||",
					_ => "||"
				};
				fullExpression += $" {op} ";
			}
			fullExpression += ConvertFilterToExpression(filter);
		}
		return fullExpression;
	}

	private static string ConvertFilterToExpression(Filter filter)
	{
		var op = filter.Operator switch
		{
			Operator.Equals => "==",
			Operator.LessThan => "<",
			Operator.LessThanOrEqual => "<=",
			Operator.GreaterThan => ">",
			Operator.GreaterThanOrEqual => ">=",
			Operator.Contains => "Contains",
			_ => "=="
		};

		if (filter.Operator == Operator.Contains)
		{
			var containsExpr = $"{filter.Left}.{op}(\"{filter.Right}\")";
			return Negate(containsExpr, filter.Negate);
		}

		var expression = $"{filter.Left} {op} ";
		if (string.IsNullOrWhiteSpace(filter.Right))
		{
			return Negate($"{filter.Left} {op} null", filter.Negate);
		}
		expression += filter.CompareFields ? filter.Right : $"\"{filter.Right}\"";
		return Negate(AllowNull(expression, filter.Left, filter.AllowNull), filter.Negate);
	}

	private static string Negate(string expression, bool negate)
	{
		return negate ? $"!({expression})" : expression;
	}

	private static string AllowNull(string expression, string field, bool allowNull)
	{
		return allowNull ? $"({field} == null || {expression})" : expression;
	}
}