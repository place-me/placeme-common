using System.Threading;
using System.Threading.Tasks;

using Dapr.Client;

using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Placeme.Common.Health;

public class DaprHealthCheck : IHealthCheck
{
	private readonly DaprClient _daprClient;

	public DaprHealthCheck(DaprClient daprClient)
	{
		_daprClient = daprClient;
	}

	public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
		CancellationToken cancellationToken = default)
	{
		var daprHealthy = await _daprClient.CheckHealthAsync(cancellationToken);

		return daprHealthy ? HealthCheckResult.Healthy() : new HealthCheckResult(context.Registration.FailureStatus,
				"Dapr sidecar unhealthy.");
	}
}