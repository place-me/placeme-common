using System.Threading;
using System.Threading.Tasks;

using EntityFrameworkCore.DbContextScope;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Placeme.Common.Health;

public class AmbientDbContextHealthCheck<TDbContext> : IHealthCheck where TDbContext : DbContext
{
	private readonly IAmbientDbContextLocator _ambientDbContextLocator;
	private readonly IDbContextScopeFactory _dbContextScopeFactory;

	public AmbientDbContextHealthCheck(IAmbientDbContextLocator ambientDbContextLocator,
		IDbContextScopeFactory dbContextScopeFactory)
	{
		_ambientDbContextLocator = ambientDbContextLocator;
		_dbContextScopeFactory = dbContextScopeFactory;
	}

	public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
		CancellationToken cancellationToken = default)
	{
		using var dbContextScope = _dbContextScopeFactory.CreateReadOnly();

		var dbContext = _ambientDbContextLocator.Get<TDbContext>();
		if (dbContext == null)
		{
			return new HealthCheckResult(context.Registration.FailureStatus,
				"DbContext could not be resolved.");
		}

		var canConnect = await dbContext.Database.CanConnectAsync(cancellationToken);

		return canConnect ? HealthCheckResult.Healthy() : new HealthCheckResult(context.Registration.FailureStatus,
				"Cannot connect to database.");
	}
}