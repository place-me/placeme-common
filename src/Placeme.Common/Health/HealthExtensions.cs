using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Placeme.Common.Health;

public static class HealthExtensions
{
	public static IHealthChecksBuilder AddAmbientDbContext<TDbContext>(this IHealthChecksBuilder builder,
	string name = "dbcontextHealthCheck") where TDbContext : DbContext
	{
		if (builder == null)
		{
			throw new ArgumentNullException(nameof(builder));
		}

		return builder.AddTypeActivatedCheck<AmbientDbContextHealthCheck<TDbContext>>(name);
	}

	public static IHealthChecksBuilder AddDaprSidecar(this IHealthChecksBuilder builder, string name = "daprSidecarHealthCheck")
	{
		if (builder == null)
		{
			throw new ArgumentNullException(nameof(builder));
		}

		return builder.AddTypeActivatedCheck<DaprHealthCheck>(name);
	}
}