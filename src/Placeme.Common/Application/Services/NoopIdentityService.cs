﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;

using Placeme.Common.Application.Interfaces;
using Placeme.Common.Application.Models;

namespace Placeme.Common.Application.Services;

public class NoopIdentityService : IIdentityService
{
	public Task<string> GetUserNameAsync(string userId)
	{
		return Task.FromResult(string.Empty);
	}

	public Task<bool> IsInRoleAsync(string userId, string role)
	{
		return Task.FromResult(true);
	}

	public Task<bool> AuthorizeAsync(string userId, string policyName)
	{
		return Task.FromResult(true);
	}

	public Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password)
	{
		return Task.FromResult((Result: new Result(true, new Collection<string>()), UserId: ""));
	}

	public Task<Result> DeleteUserAsync(string userId)
	{
		return Task.FromResult(new Result(true, new Collection<string>()));
	}
}