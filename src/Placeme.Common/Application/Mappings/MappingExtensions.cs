using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;
using AutoMapper.QueryableExtensions;

using Microsoft.EntityFrameworkCore;

using Placeme.Common.Application.Models;

namespace Placeme.Common.Application.Mappings;

public static class MappingExtensions
{
	public static Task<PaginatedList<TDestination>> PaginatedListAsync<TDestination>(
		this IQueryable<TDestination> queryable, int pageNumber, int pageSize,
		CancellationToken cancellationToken = default)
	{
		return PaginatedList<TDestination>.CreateAsync(queryable, pageNumber, pageSize, cancellationToken);
	}

	public static Task<List<TDestination>> ProjectToListAsync<TDestination>(this IQueryable queryable,
		IConfigurationProvider configuration, CancellationToken cancellationToken = default)
	{
		return queryable.ProjectTo<TDestination>(configuration).ToListAsync(cancellationToken);
	}
}