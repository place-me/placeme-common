﻿using System;

using Microsoft.Extensions.Configuration;

namespace Placeme.Common.Application;

public class PlacemeUrlProvider : IPlacemeUrlProvider
{
	private readonly string _httpSchema;
	private readonly bool _isDevMode;
	private readonly string _rootDomain;
	private readonly Uri _uri;

	public PlacemeUrlProvider(IConfiguration config)
	{
		_uri = config.GetValue<Uri>("ROOT_URL");
		_isDevMode = config.GetValue("CURRENT_USER_FROM_CONFIG", false);
		_rootDomain = $"{_uri.Host}:{_uri.Port}";
		_httpSchema = _uri.Scheme;
	}

	public Uri Portal => BuildUri("portal");

	public Uri Oidc => BuildUri("oidc");

	public Uri PortalApi => BuildUri("api.portal");

	public Uri Iam => BuildUri("iam");

	public Uri LoginClient => BuildUri("login");

	private Uri BuildUri(string subdomain)
	{
		return _isDevMode ? _uri : new Uri($"{_httpSchema}://{subdomain}.{_rootDomain}");
	}
}