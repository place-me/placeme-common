﻿using System;
using System.Collections.Generic;
using System.Linq;

using FluentValidation.Results;

namespace Placeme.Common.Application.Exceptions;

public class ValidationException : Exception
{
	public ValidationException()
		: this("One or more validation failures have occurred.")
	{
	}

	public ValidationException(IEnumerable<ValidationFailure> failures)
		: this()
	{
		Errors = failures
			.GroupBy(e => e.PropertyName, e => e.ErrorMessage)
			.ToDictionary(failureGroup => failureGroup.Key, failureGroup => failureGroup.ToArray());
	}

	public ValidationException(string? message) : this(message, null)
	{
	}

	public ValidationException(string? message, Exception? innerException) : base(message, innerException)
	{
		Errors = new Dictionary<string, string[]>();
	}

	public IDictionary<string, string[]> Errors { get; }
}