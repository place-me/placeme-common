using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Placeme.Common.Application.Query;

public static class QueryExtensions
{
	private static readonly MethodInfo OrderByMethod = typeof(Queryable).GetMethods()
		.Single(method => method.Name == "OrderBy" && method.GetParameters().Length == 2);

	private static readonly MethodInfo OrderByDescendingMethod = typeof(Queryable).GetMethods()
		.Single(method => method.Name == "OrderByDescending" && method.GetParameters().Length == 2);

	private static PropertyInfo GetProperty<T>(string propertyName)
	{
		return typeof(T).GetProperties().First(p => p.Name.Equals(propertyName, StringComparison.OrdinalIgnoreCase));
	}

	public static IQueryable<T> OrderByProperty<T>(
		this IQueryable<T> source, string propertyName)
	{
		var prop = GetProperty<T>(propertyName);
		if (prop == null)
		{
			return source;
		}

		var paramterExpression = Expression.Parameter(typeof(T));
		var orderByProperty = Expression.Property(paramterExpression, propertyName);
		var lambda = Expression.Lambda(orderByProperty, paramterExpression);
		var genericMethod = OrderByMethod.MakeGenericMethod(typeof(T), orderByProperty.Type);
		var ret = genericMethod.Invoke(null, new object[] { source, lambda });

		return ret != null ? (IQueryable<T>)ret : source;
	}

	public static IQueryable<T> OrderByPropertyDescending<T>(
		this IQueryable<T> source, string propertyName)
	{
		var prop = GetProperty<T>(propertyName);
		if (prop == null)
		{
			return source;
		}

		var paramterExpression = Expression.Parameter(typeof(T));
		var orderByProperty = Expression.Property(paramterExpression, propertyName);
		var lambda = Expression.Lambda(orderByProperty, paramterExpression);
		var genericMethod = OrderByDescendingMethod.MakeGenericMethod(typeof(T), orderByProperty.Type);
		var ret = genericMethod.Invoke(null, new object[] { source, lambda });

		return ret != null ? (IQueryable<T>)ret : source;
	}

	public static IQueryable<T> ApplySorting<T>(this IQueryable<T> source, string sorting)
	{
		if (string.IsNullOrWhiteSpace(sorting))
		{
			return source;
		}

		var requestedSortProp = sorting.TrimStart('-');
		if (sorting.StartsWith('-'))
		{
			return source.OrderByPropertyDescending(requestedSortProp);
		}

		return source.OrderByProperty(requestedSortProp);
	}
}