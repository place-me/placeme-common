﻿using System;
using System.Linq;

namespace Placeme.Common.Application.EventBus.Extensions;

public static class GenericTypeExtensions
{
	public static string GetGenericTypeName(this Type type)
	{
		if (type.IsGenericType)
		{
			var genericTypes = string.Join(",", type.GetGenericArguments().Select(t => t.Name).ToArray());
			return $"{type.Name.Remove(type.Name.IndexOf('`', StringComparison.OrdinalIgnoreCase))}<{genericTypes}>";
		}

		return type.Name;
	}

	public static string GetGenericTypeName(this object value)
	{
		return value.GetType().GetGenericTypeName();
	}
}