﻿using System.Threading.Tasks;

using Placeme.Common.Application.EventBus.Events;

namespace Placeme.Common.Application.EventBus.Abstractions;

public interface IEventBus
{
	Task PublishAsync<TIntegrationEvent>(TIntegrationEvent integrationEvent) where TIntegrationEvent : BaseIntegrationEvent;
}