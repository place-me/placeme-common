﻿using System.Threading.Tasks;

using Placeme.Common.Application.EventBus.Events;

namespace Placeme.Common.Application.EventBus.Abstractions;

public interface IIntegrationEventHandler<in TIntegrationEvent> : IIntegrationEventHandler
	where TIntegrationEvent : BaseIntegrationEvent
{
	Task Handle(TIntegrationEvent integrationEvent);
}

#pragma warning disable CA1040 // Avoid empty interfaces
public interface IIntegrationEventHandler
{
}
#pragma warning restore CA1040 // Avoid empty interfaces