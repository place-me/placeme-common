﻿using System;

namespace Placeme.Common.Application.EventBus.Events;

public abstract class BaseIntegrationEvent
{
	public Guid Id { get; } = Guid.NewGuid();
	public DateTime CreationDate { get; } = DateTime.UtcNow;
	public Guid UserprofileId { get; set; }
	public Guid TenantId { get; set; }
}