﻿using System.Threading.Tasks;

using Dapr.Client;

using Microsoft.Extensions.Logging;

using Placeme.Common.Application.EventBus.Abstractions;
using Placeme.Common.Application.EventBus.Events;
using Placeme.Common.Identity;

namespace Placeme.Common.Application.EventBus;

public class DaprEventBus : IEventBus
{
	public const string PubSubName = "pubsub";
	private readonly DaprClient _dapr;
	private readonly ILogger<DaprEventBus> _logger;
	private readonly ICurrentUser _currentUser;

	public DaprEventBus(DaprClient dapr, ILogger<DaprEventBus> logger, ICurrentUser currentUser)
	{
		_dapr = dapr;
		_logger = logger;
		_currentUser = currentUser;
	}

	public async Task PublishAsync<TIntegrationEvent>(TIntegrationEvent integrationEvent)
		where TIntegrationEvent : BaseIntegrationEvent
	{
		var topicName = integrationEvent.GetType().Name;
		integrationEvent.UserprofileId = _currentUser.UserprofileId;
		integrationEvent.TenantId = _currentUser.TenantId;

		_logger.LogInformation("Publishing event {BaseIntegrationEvent} to {PubsubName}.{TopicName}", integrationEvent, PubSubName,
			topicName);

		// We need to make sure that we pass the concrete type to PublishEventAsync,
		// which can be accomplished by casting the integrationEvent to dynamic. This ensures
		// that all integrationEvent fields are properly serialized.
		await _dapr.PublishEventAsync(PubSubName, topicName, (dynamic)integrationEvent);
	}
}