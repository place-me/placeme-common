using System;

using MediatR;

namespace Placeme.Common.Application.Commands.GetById;

public class GetByIdQuery<T> : IRequest<T>
{
	public Guid Id { get; set; }
	public string? Include { get; set; }
}