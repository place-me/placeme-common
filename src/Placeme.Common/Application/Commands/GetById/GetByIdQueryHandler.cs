﻿using System.Threading;
using System.Threading.Tasks;

using EntityFrameworkCore.DbContextScope;

using MediatR;

using Placeme.Common.Domain;
using Placeme.Common.Persistence;

namespace Placeme.Common.Application.Commands.GetById;

public class GetByIdQueryHandler<TDto, TEntity> : IRequestHandler<GetByIdQuery<TDto>, TDto>
	where TDto : class where TEntity : BaseEntity
{
	private readonly IDbContextScopeFactory _dbContextScopeFactory;
	private readonly IRepository<TEntity> _repository;

	public GetByIdQueryHandler(IDbContextScopeFactory dbContextScopeFactory, IRepository<TEntity> repository)
	{
		_dbContextScopeFactory = dbContextScopeFactory;
		_repository = repository;
	}

	public async Task<TDto> Handle(GetByIdQuery<TDto> request, CancellationToken cancellationToken)
	{
		using var dbContextScope = _dbContextScopeFactory.CreateReadOnly();

		var include = request.Include ?? string.Empty;
		var dto = await _repository.GetAsync<TDto>(request.Id, false, cancellationToken, include.ToIncludeExpressions<TEntity>());

		return dto!;
	}
}