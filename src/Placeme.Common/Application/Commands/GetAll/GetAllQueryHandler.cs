using System;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

using EntityFrameworkCore.DbContextScope;

using MediatR;

using Placeme.Common.Application.Interfaces;
using Placeme.Common.Application.Models;
using Placeme.Common.Domain;
using Placeme.Common.Persistence;

using Serilog;

namespace Placeme.Common.Application.Commands.GetAll;

public class GetAllQueryHandler<TDto, TEntity> : IRequestHandler<GetAllQuery<TDto>, PaginatedList<TDto>>
	where TEntity : BaseEntity
{
	private readonly IDbContextScopeFactory _dbContextScopeFactory;
	private readonly IRepository<TEntity> _repository;
	private readonly IFilterParser _filterParser;

	public GetAllQueryHandler(IDbContextScopeFactory dbContextScopeFactory, IRepository<TEntity> repository, IFilterParser filterParser)
	{
		_dbContextScopeFactory = dbContextScopeFactory;
		_repository = repository;
		_filterParser = filterParser;
	}

	public async Task<PaginatedList<TDto>> Handle(GetAllQuery<TDto> request, CancellationToken cancellationToken)
	{
		if (request == null)
		{
			return await Task.FromResult(new PaginatedList<TDto>(new Collection<TDto>(), 0, 0, 0));
		}

		using var dbContextScope = _dbContextScopeFactory.CreateReadOnly();

		Expression<Func<TEntity, bool>>? filter = null;
		if (request.ListRequest.FilterSet?.Filters != null && request.ListRequest.FilterSet.Filters.Count > 0)
		{
			try
			{
				filter = _filterParser.ConvertToLinqExpression<TEntity>(request.ListRequest.FilterSet.Filters, request.ListRequest.FilterSet.LogicalOperator);
			}
			catch (Exception ex)
			{
				Log.Error(ex, $"Filter could not be parsed '{request.ListRequest.FilterSet.Filters}'");
				throw;
			}
		}
		var sort = string.Empty;
		if (request.ListRequest.Sort != null)
		{
			sort = (request.ListRequest.Sort.IsDescending ? "-" : string.Empty) + request.ListRequest.Sort.SortElement;
		}

		return await _repository.GetAsync<TDto>(noTracking: true,
			orderByString: sort,
			pageNumber: request.ListRequest.PageNumber,
			pageSize: request.ListRequest.PageSize,
			filter: filter,
			includes: request.ListRequest.Include.ToIncludeExpressions<TEntity>(),
			cancellationToken: cancellationToken);
	}
}