using MediatR;

using Placeme.Common.Application.Models;
using Placeme.Common.Generated.Base;

namespace Placeme.Common.Application.Commands.GetAll;

public class GetAllQuery<T> : IRequest<PaginatedList<T>>
{
	public ListRequest ListRequest { get; set; } = new ListRequest();
}