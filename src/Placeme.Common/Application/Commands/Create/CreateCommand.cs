using MediatR;

namespace Placeme.Common.Application.Commands.Create;

public class CreateCommand<T> : IRequest<T>
{
	public T? Data { get; set; }
}