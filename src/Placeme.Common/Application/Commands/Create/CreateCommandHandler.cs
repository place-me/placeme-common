using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using EntityFrameworkCore.DbContextScope;

using MediatR;

using Placeme.Common.Domain;
using Placeme.Common.Identity;
using Placeme.Common.Persistence;

namespace Placeme.Common.Application.Commands.Create;

public class CreateCommandHandler<TDto, TEntity> : IRequestHandler<CreateCommand<TDto>, TDto>
	where TDto : class where TEntity : BaseEntity
{
	private readonly ICurrentUser _currentUser;
	private readonly IDbContextScopeFactory _dbContextScopeFactory;
	private readonly IMapper _mapper;
	private readonly IRepository<TEntity> _repository;

	public CreateCommandHandler(IDbContextScopeFactory dbContextScopeFactory, IRepository<TEntity> repository,
		IMapper mapper, ICurrentUser currentUser)
	{
		_dbContextScopeFactory = dbContextScopeFactory;
		_repository = repository;
		_mapper = mapper;
		_currentUser = currentUser;
	}

	public async Task<TDto> Handle(CreateCommand<TDto> request, CancellationToken cancellationToken)
	{
		using var dbContextScope = _dbContextScopeFactory.Create();
		var entity = _mapper.Map<TEntity>(request.Data, options => options.AddCurrentUser(_currentUser));
		await _repository.AddAsync(entity, cancellationToken);
		await dbContextScope.SaveChangesAsync(cancellationToken);

		return (TDto)_mapper.Map(entity, typeof(TEntity), typeof(TDto));
	}
}