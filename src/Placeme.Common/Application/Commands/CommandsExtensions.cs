﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using AutoMapper;

using MediatR;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

using Placeme.Common.Api.Parsers;
using Placeme.Common.Application.Commands.Create;
using Placeme.Common.Application.Commands.Delete;
using Placeme.Common.Application.Commands.GetAll;
using Placeme.Common.Application.Commands.GetById;
using Placeme.Common.Application.Commands.Update;
using Placeme.Common.Application.Interfaces;
using Placeme.Common.Application.Mappings;
using Placeme.Common.Application.Models;
using Placeme.Common.Domain;
using Placeme.Common.Identity;

namespace Placeme.Common.Application.Commands;

public static class CommandsExtensions
{
	public static void AddCrudCommands(this IServiceCollection services, params Assembly[] assemblies)
	{
		foreach (var assembly in assemblies)
		{
			var dtoImplementations = assembly.GetTypes().Where(t =>
			{
				return t.GetInterfaces()
					.Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IMapFrom<>));
			});

			foreach (var dtoImplementation in dtoImplementations)
			{
				var pagedList = typeof(PaginatedList<>).MakeGenericType(dtoImplementation);
				var dtoInterface = Array.Find(dtoImplementation.GetInterfaces(), i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>));

				if (dtoInterface == null || dtoInterface.GenericTypeArguments.Length != 1)
				{
					continue;
				}

				var baseEntity = dtoInterface.GenericTypeArguments[0];

				// Ensure that the generic parameter is of type BaseEntity
				if (!typeof(BaseEntity).IsAssignableFrom(baseEntity))
				{
					continue;
				}

				var getAllQuery = typeof(GetAllQuery<>).MakeGenericType(dtoImplementation);
				var getAllRequestHandlerInterface = typeof(IRequestHandler<,>).MakeGenericType(getAllQuery, pagedList);
				var getAllRequestHandler = typeof(GetAllQueryHandler<,>).MakeGenericType(dtoImplementation, baseEntity);

				var getByIdQuery = typeof(GetByIdQuery<>).MakeGenericType(dtoImplementation);
				var getByIdRequestHandlerInterface =
					typeof(IRequestHandler<,>).MakeGenericType(getByIdQuery, dtoImplementation);
				var getByIdRequestHandler =
					typeof(GetByIdQueryHandler<,>).MakeGenericType(dtoImplementation, baseEntity);

				var createCommand = typeof(CreateCommand<>).MakeGenericType(dtoImplementation);
				var createRequestHandlerInterface =
					typeof(IRequestHandler<,>).MakeGenericType(createCommand, dtoImplementation);
				var createRequestHandler =
					typeof(CreateCommandHandler<,>).MakeGenericType(dtoImplementation, baseEntity);

				var updateCommand = typeof(UpdateCommand<>).MakeGenericType(dtoImplementation);
				var updateRequestHandlerInterface =
					typeof(IRequestHandler<,>).MakeGenericType(updateCommand, dtoImplementation);
				var updateRequestHandler =
					typeof(UpdateCommandHandler<,>).MakeGenericType(dtoImplementation, baseEntity);

				var deleteCommand = typeof(DeleteCommand<>).MakeGenericType(dtoImplementation);
				var deleteRequestHandlerInterface =
					typeof(IRequestHandler<,>).MakeGenericType(deleteCommand, dtoImplementation);
				var deleteRequestHandler =
					typeof(DeleteCommandHandler<,>).MakeGenericType(dtoImplementation, baseEntity);

				services.TryAddTransient(getAllRequestHandlerInterface, getAllRequestHandler);
				services.TryAddTransient(getByIdRequestHandlerInterface, getByIdRequestHandler);
				services.TryAddTransient(createRequestHandlerInterface, createRequestHandler);
				services.TryAddTransient(updateRequestHandlerInterface, updateRequestHandler);
				services.TryAddTransient(deleteRequestHandlerInterface, deleteRequestHandler);
			}
		}
		services.TryAddTransient(typeof(IFilterParser), typeof(FilterToLinqParser));
	}

	public static void AddCurrentUser(this IMappingOperationOptions options, ICurrentUser currentUser)
	{
		options.Items.Add(Constants.CurrentUserItem, currentUser);
	}

	public static Expression<Func<TDto, object>>[] ToIncludeExpressions<TDto>(this string? includeParameter)
	{
		if (string.IsNullOrWhiteSpace(includeParameter))
		{
			return Array.Empty<Expression<Func<TDto, object>>>();
		}

		var includes = includeParameter.Split(',').Where(_ => !string.IsNullOrWhiteSpace(_)).Select(_ => _.Trim());
		if (!includes.Any())
		{
			return Array.Empty<Expression<Func<TDto, object>>>();
		}

		var expressions = new List<Expression<Func<TDto, object>>>();

		foreach (var include in includes)
		{
			var prop = GetProperty<TDto>(include);
			if (prop == null)
			{
				continue;
			}

			var parameterExpression = Expression.Parameter(typeof(TDto));
			var includeExpression = Expression.Property(parameterExpression, prop);
			var lambda = Expression.Lambda<Func<TDto, object>>(includeExpression, parameterExpression);
			expressions.Add(lambda);
		}

		return expressions.ToArray();
	}

	private static PropertyInfo? GetProperty<T>(string propertyName)
	{
		return Array.Find(typeof(T).GetProperties(), p => p.Name.Equals(propertyName, StringComparison.OrdinalIgnoreCase));
	}
}