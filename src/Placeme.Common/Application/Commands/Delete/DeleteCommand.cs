using System;

using MediatR;

namespace Placeme.Common.Application.Commands.Delete;

public class DeleteCommand<T> : IRequest<T> where T : class
{
	public Guid Id { get; set; }
}