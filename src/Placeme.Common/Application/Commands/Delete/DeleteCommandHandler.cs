using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using EntityFrameworkCore.DbContextScope;

using MediatR;

using Placeme.Common.Application.Exceptions;
using Placeme.Common.Domain;
using Placeme.Common.Persistence;

namespace Placeme.Common.Application.Commands.Delete;

public class DeleteCommandHandler<TDto, TEntity> : IRequestHandler<DeleteCommand<TDto>, TDto>
	where TDto : class where TEntity : BaseEntity
{
	private readonly IDbContextScopeFactory _dbContextScopeFactory;
	private readonly IMapper _mapper;
	private readonly IRepository<TEntity> _repository;

	public DeleteCommandHandler(IDbContextScopeFactory dbContextScopeFactory, IRepository<TEntity> repository,
		IMapper mapper)
	{
		_dbContextScopeFactory = dbContextScopeFactory;
		_repository = repository;
		_mapper = mapper;
	}

	public async Task<TDto> Handle(DeleteCommand<TDto> request, CancellationToken cancellationToken)
	{
		using var dbContextScope = _dbContextScopeFactory.Create();
		var entity = await _repository.GetAsync(request.Id, cancellationToken: cancellationToken);
		if (entity == null)
		{
			throw new NotFoundException($"Could not find entity of type {typeof(TDto).Name} with id {request.Id}");
		}

		_repository.Delete(entity);
		await dbContextScope.SaveChangesAsync(cancellationToken);

		return (TDto)_mapper.Map(entity, typeof(TEntity), typeof(TDto));
	}
}