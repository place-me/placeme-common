using System;

using Google.Protobuf.WellKnownTypes;

using MediatR;

namespace Placeme.Common.Application.Commands.Update;

public class UpdateCommand<T> : IRequest<T>
{
	public UpdateCommand(T data, FieldMask updateMask)
	{
		Data = data;
		UpdateMask = updateMask;
	}
	public Guid Id { get; set; }
	public T Data { get; private set; }

	public FieldMask UpdateMask { get; }
}