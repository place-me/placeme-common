using System;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using EntityFrameworkCore.DbContextScope;

using Google.Protobuf;

using MediatR;

using Placeme.Common.Application.Exceptions;
using Placeme.Common.Domain;
using Placeme.Common.Identity;
using Placeme.Common.Persistence;

using Serilog;

using static Google.Protobuf.WellKnownTypes.FieldMask;

namespace Placeme.Common.Application.Commands.Update;

public class UpdateCommandHandler<TDto, TEntity> : IRequestHandler<UpdateCommand<TDto>, TDto>
	where TDto : class, IMessage where TEntity : BaseEntity
{
	private readonly ICurrentUser _currentUser;
	private readonly IDbContextScopeFactory _dbContextScopeFactory;
	private readonly IMapper _mapper;
	private readonly IRepository<TEntity> _repository;

	public UpdateCommandHandler(IDbContextScopeFactory dbContextScopeFactory, IRepository<TEntity> repository,
		IMapper mapper, ICurrentUser currentUser)
	{
		_dbContextScopeFactory = dbContextScopeFactory;
		_repository = repository;
		_mapper = mapper;
		_currentUser = currentUser;
	}

	public async Task<TDto> Handle(UpdateCommand<TDto> request, CancellationToken cancellationToken)
	{
		using var dbContextScope = _dbContextScopeFactory.Create();
		var entity = await _repository.GetAsync(request.Id, cancellationToken: cancellationToken);
		if (entity == null)
		{
			throw new NotFoundException($"Could not find entity of type {typeof(TDto).Name} with id {request.Id}");
		}

		// Check if we have to update something, if not return entity immediately
		if (request.UpdateMask == null || request.UpdateMask.Paths.Count == 0)
		{
			return (TDto)_mapper.Map(entity, typeof(TEntity), typeof(TDto));
		}

		var existingMessage = _mapper.Map<TDto>(entity);
		try
		{
			request.UpdateMask.Merge(request.Data, existingMessage, new MergeOptions() { ReplaceRepeatedFields = true });
			_mapper.Map(existingMessage, entity, options => options.AddCurrentUser(_currentUser));
		}
		catch (Exception e)
		{
			Log.Error(e, "Failed to merge FilterMask");
			_mapper.Map(request.Data, entity, options => options.AddCurrentUser(_currentUser));
		}

		await dbContextScope.SaveChangesAsync(cancellationToken);

		return (TDto)_mapper.Map(entity, typeof(TEntity), typeof(TDto));
	}
}