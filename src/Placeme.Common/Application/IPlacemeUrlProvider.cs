﻿using System;

namespace Placeme.Common.Application;

public interface IPlacemeUrlProvider
{
	Uri Iam { get; }

	Uri LoginClient { get; }

	Uri Oidc { get; }

	Uri PortalApi { get; }

	Uri Portal { get; }
}