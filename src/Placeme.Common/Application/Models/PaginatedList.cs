using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

namespace Placeme.Common.Application.Models;

public class PaginatedList<T>
{
	public PaginatedList(IEnumerable<T> items, int totalCount, int pageNumber, int pageSize)
	{
		PageNumber = pageSize >= totalCount ? 1 : pageNumber;
		TotalPages = pageSize > 0 ? (int)Math.Ceiling(totalCount / (double)pageSize) : 0;
		TotalCount = totalCount;
		Items = items;
	}

	public bool HasNextPage => PageNumber < TotalPages;

	public bool HasPreviousPage => PageNumber > 1;

	public IEnumerable<T> Items { get; }

	public int PageNumber { get; }

	public int TotalCount { get; }

	public int TotalPages { get; }

	public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> source, int pageNumber, int pageSize,
		CancellationToken cancellationToken = default)
	{
		List<T> items;
		pageNumber = Math.Max(1, pageNumber);
		if (pageSize > 0)
		{
			var skipPages = Math.Max(0, pageNumber - 1);
			items = await source.Skip(skipPages * pageSize).Take(pageSize).ToListAsync(cancellationToken);
		}
		else
		{
			var count = await source.CountAsync(cancellationToken);
			items = await source.ToListAsync(cancellationToken);
			pageSize = count;
		}

		return new PaginatedList<T>(items, items.Count, pageNumber, pageSize);
	}
}