using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Placeme.Common.Application.Models;

/// <summary>
///     Helper class to deserialize, serialized <see cref="PaginatedList{T}" />
/// </summary>
public class PaginatedListDto<T>
{
	public IEnumerable<T> Items { get; set; } = new Collection<T>();
	public int PageIndex { get; set; }
	public int TotalPages { get; set; }
	public int TotalCount { get; set; }
	public bool HasPreviousPage { get; set; }
	public bool HasNextPage { get; set; }
}