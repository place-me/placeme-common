﻿using System.Threading;
using System.Threading.Tasks;

using MediatR.Pipeline;

using Microsoft.Extensions.Logging;

using Placeme.Common.Application.Interfaces;
using Placeme.Common.Identity;

namespace Placeme.Common.Application.Behaviors;

public class LoggingBehavior<TRequest> : IRequestPreProcessor<TRequest> where TRequest : notnull
{
	private readonly ICurrentUser _currentUser;
	private readonly IIdentityService _identityService;
	private readonly ILogger _logger;

	public LoggingBehavior(ILogger<TRequest> logger, ICurrentUser currentUser,
		IIdentityService identityService)
	{
		_logger = logger;
		_currentUser = currentUser;
		_identityService = identityService;
	}

	public async Task Process(TRequest request, CancellationToken cancellationToken)
	{
		var requestName = typeof(TRequest).Name;
		var userId = _currentUser.UserprofileId.ToString() ?? string.Empty;
		var userName = string.Empty;

		if (!string.IsNullOrEmpty(userId))
		{
			userName = await _identityService.GetUserNameAsync(userId);
		}

		_logger.LogInformation("userprofile_service Request: {Name} {@UserId} {@UserName} {@Request}",
			requestName, userId, userName, request);
	}
}