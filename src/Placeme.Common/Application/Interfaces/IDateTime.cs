using System;

namespace Placeme.Common.Application.Interfaces;

public interface IDateTime
{
	/// <summary>
	///     Get the current date and time in UTC format
	/// </summary>
	DateTime GetUtcNow();
}