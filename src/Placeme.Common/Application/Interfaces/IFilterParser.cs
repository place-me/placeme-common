using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using Placeme.Common.Generated.Base;

namespace Placeme.Common.Application.Interfaces;

public interface IFilterParser
{
	public Expression<Func<T, bool>> ConvertToLinqExpression<T>(IEnumerable<Filter> filters, LogicalOperator logicalOperator);
}