using System;

using FluentValidation;
using FluentValidation.Validators;

namespace Placeme.Common.Application.Validators;

public class GuidValidator<T, TProperty> : PropertyValidator<T, TProperty>
{
	public override string Name => "GuidValidator";

	public override bool IsValid(ValidationContext<T> context, TProperty value)
	{
		if (value == null)
		{
			return false;
		}

		return Guid.TryParse(value.ToString(), out var guid) && guid != Guid.Empty;
	}
}