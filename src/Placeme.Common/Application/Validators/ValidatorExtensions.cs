using FluentValidation;

namespace Placeme.Common.Application.Validators;

public static class ValidatorExtensions
{
	public static IRuleBuilderOptions<T, TGuid> IsGuid<T, TGuid>(this IRuleBuilder<T, TGuid> ruleBuilder)
	{
		return ruleBuilder.SetValidator(new GuidValidator<T, TGuid>());
	}
}