﻿using System;

namespace Placeme.Common.Domain;

public abstract class BaseEntity
{
	public Guid Id { get; set; }
}