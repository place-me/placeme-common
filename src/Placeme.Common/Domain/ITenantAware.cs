using System;

namespace Placeme.Common.Domain;

public interface ITenantAware
{
	Guid TenantId { get; set; }
}