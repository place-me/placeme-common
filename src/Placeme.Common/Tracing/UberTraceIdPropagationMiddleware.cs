﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using Serilog;

namespace Placeme.Common.Tracing;

/// <summary>
///     Middleware which converts the uber-trace-id header to a W3C compliant format and sets the correct activity parent
///     context.
///     This is required since ingress-nginx generates trace-id in the uber-trace-id format.
///     The W3C format is specified at https://www.w3.org/TR/trace-context .
/// </summary>
public class UberTraceIdPropagationMiddleware
{
	private readonly RequestDelegate _next;

	public UberTraceIdPropagationMiddleware(RequestDelegate next)
	{
		_next = next;
	}

	public async Task InvokeAsync(HttpContext context)
	{
		const string UberTraceIdName = "uber-trace-id";

		if (context.Request.Headers.TryGetValue(UberTraceIdName, out var traceValue))
		{
			var headerValue = traceValue.FirstOrDefault();
			if (headerValue is null || Activity.Current is null)
			{
				await _next(context);
				return;
			}

			Log.Logger.Debug("uber-trace-id received from nginx {UberTraceId}", headerValue);

			// uber-trace-id: {trace-id}:{span-id}:{parent-span-id}:{flags}
			var uberTraceParts = headerValue.Split(":");
			if (uberTraceParts.Length < 3)
			{
				await _next(context);
				return;
			}

			var traceId = uberTraceParts[0];
			var spanId = uberTraceParts[1];

			// The parent-span_id is deprecated, most Jaeger clients ignore on the receiving side,
			// but still include it on the sending side.
			// If omitted by the sending side the index of the flags part is lowered
			var flagsIndex = Math.Max(2, uberTraceParts.Length - 1);
			var flags = uberTraceParts[flagsIndex];

			try
			{
				var activityTraceId = ActivityTraceId.CreateFromString(traceId.PadLeft(32, '0'));
				var activitySpanId = ActivitySpanId.CreateFromString(spanId.PadLeft(16, '0'));
				if (!Enum.TryParse<ActivityTraceFlags>(flags, true, out var activityFlagsId))
				{
					Log.Warning("Unable to parse ActivityTraceFlags {Flags}", flags);
				}

				Activity.Current.SetParentId(activityTraceId, activitySpanId, activityFlagsId);
			}
			catch (ArgumentOutOfRangeException e)
			{
				Log.Logger.Error("Unable to set Activity.ParentId(). {Error}", e);
			}
		}
		else
		{
			Log.Logger.Warning("No uber-trace-id found in request header collection {Headers}",
				context.Request.Headers);
		}

		await _next(context);
	}
}