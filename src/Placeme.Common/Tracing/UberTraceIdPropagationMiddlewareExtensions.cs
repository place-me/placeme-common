﻿using Microsoft.AspNetCore.Builder;

namespace Placeme.Common.Tracing;

/// <summary>
///     UberTraceIdPropagation extensions.
/// </summary>
public static class UberTraceIdPropagationMiddlewareExtensions
{
	/// <summary>
	///     Extension method to setup the UberTraceIdPropagationMiddleware.
	/// </summary>
	public static IApplicationBuilder UseUberTraceIdPropagation(this IApplicationBuilder builder)
	{
		return builder.UseMiddleware<UberTraceIdPropagationMiddleware>();
	}
}