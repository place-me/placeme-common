﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

using Placeme.Common.Application.Models;
using Placeme.Common.Domain;

namespace Placeme.Common.Persistence;

public interface IRepository<TEntity> where TEntity : BaseEntity
{
	bool All(Expression<Func<TEntity, bool>> p,
		Expression<Func<TEntity, bool>>? filter = null,
		bool ignoreQueryFilter = false);

	bool Any(Expression<Func<TEntity, bool>> predicate, bool ignoreQueryFilter = false);

	Task<TEntity?> GetAsync(Guid uuid, bool ignoreQueryFilter = false, CancellationToken cancellationToken = default,
		params Expression<Func<TEntity, object>>[] includes);

	Task<PaginatedList<TEntity>> GetAsync(Expression<Func<TEntity, bool>>? filter = null,
		Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderByFunc = null,
		string? orderByString = null,
		bool noTracking = false,
		bool ignoreQueryFilter = false,
		int pageNumber = 1,
		int pageSize = 100,
		CancellationToken cancellationToken = default,
		params Expression<Func<TEntity, object>>[] includes);

	Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> filter,
		bool noTracking = false,
		bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

	Task<TEntity?> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> filter,
		bool noTracking = false,
		bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

	// ################################## PROJECTIONS ################################
	Task<TProjection?> GetAsync<TProjection>(Guid uuid, bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

	Task<PaginatedList<TProjection>> GetAsync<TProjection>(Expression<Func<TEntity, bool>>? filter = null,
		Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderByFunc = null,
		string? orderByString = null,
		bool noTracking = false,
		bool ignoreQueryFilter = false,
		int pageNumber = 1,
		int pageSize = 100,
		CancellationToken cancellationToken = default,
		params Expression<Func<TEntity, object>>[] includes);

	Task<TProjection> SingleAsync<TProjection>(Expression<Func<TEntity, bool>> filter,
		bool noTracking = false,
		bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

	Task<TProjection?> SingleOrDefaultAsync<TProjection>(Expression<Func<TEntity, bool>> filter,
		bool noTracking = false,
		bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes);

	void Delete(TEntity entityToDelete);

	void Delete(object id);

	Task AddAsync(TEntity entity, CancellationToken cancellationToken = default);

	void Update(TEntity entityToUpdate);
}