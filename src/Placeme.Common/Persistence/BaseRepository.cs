﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using EntityFrameworkCore.DbContextScope;

using Microsoft.EntityFrameworkCore;

using Placeme.Common.Application.Exceptions;
using Placeme.Common.Application.Mappings;
using Placeme.Common.Application.Models;
using Placeme.Common.Application.Query;
using Placeme.Common.Domain;

namespace Placeme.Common.Persistence;

public class BaseRepository<TDbContext, TEntity> : IRepository<TEntity>
	where TDbContext : DbContext where TEntity : BaseEntity
{
	private readonly IAmbientDbContextLocator _ambientDbContextLocator;
	private readonly IMapper _mapper;

	public BaseRepository(IAmbientDbContextLocator ambientDbContextLocator, IMapper mapper)
	{
		_ambientDbContextLocator = ambientDbContextLocator ??
								   throw new ArgumentNullException(nameof(ambientDbContextLocator));
		_mapper = mapper;
	}

	private DbSet<TEntity> DbSet => DbContext.Set<TEntity>();

	protected TDbContext DbContext
	{
		get
		{
			var dbContext = _ambientDbContextLocator.Get<TDbContext>();
			if (dbContext == null)
			{
				throw new InvalidOperationException(
					$"No ambient DbContext of type {typeof(TDbContext).Name} found. This means "
					+ "that this repository method has been called outside of the scope of a DbContextScope. "
					+ "A repository must only be accessed within the scope of a DbContextScope, which takes "
					+ "care of creating the DbContext instances that the repositories need and making them "
					+ "available as ambient contexts. This is what ensures that, for any given DbContext-derived "
					+ "type, the same instance is used throughout the duration of a business transaction. "
					+ "To fix this issue, use IDbContextScopeFactory in your top-level business logic service "
					+ "method to create a DbContextScope that wraps the entire business transaction that your "
					+ "service method implements. Then access this repository within that scope. Refer to the "
					+ "comments in the IDbContextScope.cs file for more details.");
			}

			return dbContext;
		}
	}

	public bool All(Expression<Func<TEntity, bool>> p, Expression<Func<TEntity, bool>>? filter = null,
		bool ignoreQueryFilter = false)
	{
		IQueryable<TEntity> queryable = DbSet;
		queryable = ApplyQueryableOperations(queryable, filter, null, null, true, ignoreQueryFilter);

		return queryable.All(p);
	}

	public bool Any(Expression<Func<TEntity, bool>> predicate, bool ignoreQueryFilter = false)
	{
		IQueryable<TEntity> queryable = DbSet;
		queryable = ApplyQueryableOperations(queryable, null, null, null, true, ignoreQueryFilter);

		return queryable.Any(predicate);
	}

	public void Delete(TEntity entityToDelete)
	{
		if (DbContext.Entry(entityToDelete).State == EntityState.Detached)
		{
			DbSet.Attach(entityToDelete);
		}

		DbSet.Remove(entityToDelete);
	}

	public void Delete(object id)
	{
		var entityToDelete = DbSet.Find(id);
		if (entityToDelete == null)
		{
			throw new NotFoundException(nameof(TEntity), id);
		}

		Delete(entityToDelete);
	}

	public virtual Task AddAsync(TEntity entity, CancellationToken cancellationToken = default)
	{
		return DbSet.AddAsync(entity, cancellationToken).AsTask();
	}

	public void Update(TEntity entityToUpdate)
	{
		DbSet.Attach(entityToUpdate);
		DbContext.Entry(entityToUpdate).State = EntityState.Modified;
	}

	private static IQueryable<TEntity> ApplyQueryableOperations(IQueryable<TEntity> query,
		Expression<Func<TEntity, bool>>? filter,
		Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderByFunc,
		string? orderByString,
		bool noTracking,
		bool ignoreQueryFilters,
		params Expression<Func<TEntity, object>>[]? includes)
	{
		if (noTracking)
		{
			query = query.AsNoTracking();
		}

		if (filter != null)
		{
			query = query.Where(filter);
		}

		if (!string.IsNullOrEmpty(orderByString))
		{
			query = query.ApplySorting(orderByString);
		}

		if (orderByFunc != null)
		{
			query = orderByFunc(query);
		}

		if (ignoreQueryFilters)
		{
			query = query.IgnoreQueryFilters();
		}

		if (includes?.Any() == true)
		{
			foreach (var include in includes)
			{
				query = query.Include(include);
			}
		}

		return query;
	}

	#region GET ENTITIES

	public Task<TEntity?> GetAsync(Guid uuid, bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default,
		params Expression<Func<TEntity, object>>[] includes)
	{
		return SingleOrDefaultAsync(e => e.Id == uuid, ignoreQueryFilter: ignoreQueryFilter, includes: includes,
			cancellationToken: cancellationToken);
	}

	public Task<PaginatedList<TEntity>> GetAsync(Expression<Func<TEntity, bool>>? filter = null,
		Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderByFunc = null, string? orderByString = null,
		bool noTracking = false,
		bool ignoreQueryFilter = false, int pageNumber = 0, int pageSize = 100,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
	{
		IQueryable<TEntity> query = DbSet;
		query = ApplyQueryableOperations(query, filter, orderByFunc, orderByString, noTracking, ignoreQueryFilter,
			includes);

		return query.PaginatedListAsync(pageNumber, pageSize, cancellationToken);
	}

	public Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> filter, bool noTracking = false,
		bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
	{
		IQueryable<TEntity> query = DbSet;
		query = ApplyQueryableOperations(query, filter, null, null, noTracking, ignoreQueryFilter, includes);
		return query.SingleAsync(filter, cancellationToken);
	}

	public Task<TEntity?> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> filter, bool noTracking = false,
		bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
	{
		IQueryable<TEntity> query = DbSet;
		query = ApplyQueryableOperations(query, filter, null, null, noTracking, ignoreQueryFilter, includes);

		return query.SingleOrDefaultAsync(filter, cancellationToken);
	}

	#endregion

	#region GET PROJECTIONS

	public Task<TProjection?> GetAsync<TProjection>(Guid uuid, bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default,
		params Expression<Func<TEntity, object>>[] includes)
	{
		return SingleOrDefaultAsync<TProjection>(e => e.Id == uuid, ignoreQueryFilter: ignoreQueryFilter,
			includes: includes, cancellationToken: cancellationToken);
	}

	public async Task<PaginatedList<TProjection>> GetAsync<TProjection>(Expression<Func<TEntity, bool>>? filter = null,
		Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderByFunc = null, string? orderByString = null,
		bool noTracking = false,
		bool ignoreQueryFilter = false, int pageNumber = 0, int pageSize = 100,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
	{
		IQueryable<TEntity> query = DbSet;
		query = ApplyQueryableOperations(query, filter, orderByFunc, orderByString, noTracking, ignoreQueryFilter, includes);
		var pagedList = await query.PaginatedListAsync(pageNumber, pageSize, cancellationToken);
		var projectedItems = pagedList.Items.ToList().ConvertAll(i => _mapper.Map<TProjection>(i));
		return new PaginatedList<TProjection>(projectedItems, pagedList.TotalCount, pagedList.PageNumber, projectedItems.Count);
	}

	public async Task<TProjection> SingleAsync<TProjection>(Expression<Func<TEntity, bool>> filter,
		bool noTracking = false,
		bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
	{
		IQueryable<TEntity> query = DbSet;
		query = ApplyQueryableOperations(query, filter, null, null, noTracking, ignoreQueryFilter, includes);
		var entity = await query.SingleAsync(cancellationToken);
		return _mapper.Map<TProjection>(entity);
	}

	public async Task<TProjection?> SingleOrDefaultAsync<TProjection>(Expression<Func<TEntity, bool>> filter,
		bool noTracking = false,
		bool ignoreQueryFilter = false,
		CancellationToken cancellationToken = default, params Expression<Func<TEntity, object>>[] includes)
	{
		IQueryable<TEntity> query = DbSet;
		query = ApplyQueryableOperations(query, filter, null, null, noTracking, ignoreQueryFilter, includes);
		var entity = await query.SingleOrDefaultAsync(cancellationToken);
		return _mapper.Map<TProjection>(entity);
	}

	#endregion
}