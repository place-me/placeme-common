﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using Placeme.Common.Application.Interfaces;
using Placeme.Common.Domain;
using Placeme.Common.Identity;

using Serilog;

namespace Placeme.Common.Persistence;

public abstract class BaseDbContext : DbContext
{
	// Applying BaseEntity rules to all entities that inherit from it.
	// Define MethodInfo member that is used when model is built.
	private static readonly MethodInfo SetGlobalQueryMethod = typeof(BaseDbContext)
		.GetMethods(BindingFlags.Public | BindingFlags.Instance)
		.Single(t => t.IsGenericMethod && t.Name == "ApplyGlobalQueryFilters");

	private readonly ICurrentUser _currentUser;
	private readonly IDateTime _dateTime;

	protected BaseDbContext(DbContextOptions options, ICurrentUser currentUser, IDateTime dateTime)
		: base(options)
	{
		_currentUser = currentUser;
		_dateTime = dateTime;
	}

	/// <summary>
	///     This must return the assembly which contains the EntityTypeConfigurations.
	/// </summary>
	protected abstract Assembly EntityConfigurationAssembly { get; }

	private static IEnumerable<TypeInfo> GetConstructableTypes(Assembly assembly)
	{
		return GetLoadableDefinedTypes(assembly).Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition);
	}

	private static IEnumerable<TypeInfo> GetLoadableDefinedTypes(Assembly assembly)
	{
		try
		{
			return assembly.DefinedTypes;
		}
		catch (ReflectionTypeLoadException ex)
		{
			return ex.Types.Where(t => t != null).Select(IntrospectionExtensions.GetTypeInfo!);
		}
	}

	// This method is called for every loaded entity type in OnModelCreating method.
	public void ApplyGlobalQueryFilters<TEntity>(ModelBuilder builder) where TEntity : AuditableEntity
	{
		builder.Entity<TEntity>().HasQueryFilter(e => ((ITenantAware)e).TenantId == _currentUser.TenantId);
	}

	public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
	{
		foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
		{
			switch (entry.State)
			{
				case EntityState.Added:
					entry.Entity.CreatedBy = _currentUser.UserprofileId.ToString();
					entry.Entity.Created = _dateTime.GetUtcNow();
					break;

				case EntityState.Modified:
					entry.Entity.LastModifiedBy = _currentUser.UserprofileId.ToString();
					entry.Entity.LastModified = _dateTime.GetUtcNow();
					break;
			}
		}

		return await base.SaveChangesAsync(cancellationToken);
	}

	public virtual ModelBuilder ApplyConfigurationsFromAssembly(ModelBuilder modelBuilder)
	{
		static bool IsEntityTypeConfiguration(Type i)
		{
			return i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>);
		}

		var methodInfo = typeof(ModelBuilder)
			.GetMethods()
			.Single(e => e.Name == "ApplyConfiguration"
						 && e.ContainsGenericParameters
						 && e.GetParameters().SingleOrDefault()?.ParameterType.GetGenericTypeDefinition() ==
						 typeof(IEntityTypeConfiguration<>));

		GetConstructableTypes(EntityConfigurationAssembly)
			.ToList()
			.ForEach(entityConfig => entityConfig.GetInterfaces()
				.Where(IsEntityTypeConfiguration)
				.ToList()
				.ForEach(i =>
				{
					var genericType = i.GenericTypeArguments[0];
					methodInfo.MakeGenericMethod(genericType).Invoke(modelBuilder,
						new[] { Activator.CreateInstance(entityConfig) });

#pragma warning disable RCS1208 // Reduce 'if' nesting
					if (genericType.IsAssignableTo(typeof(ITenantAware)))
					{
						var method = SetGlobalQueryMethod.MakeGenericMethod(genericType);
						method.Invoke(this, new object[] { modelBuilder });

						Log.Information("Applying global tenant Filer to {type}", genericType.Name);
					}
#pragma warning restore RCS1208
				})
			);
		return modelBuilder;
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		// TODO: Should we cache this Config evaluation?
		ApplyConfigurationsFromAssembly(modelBuilder);

		var dateTimeConverter = new ValueConverter<DateTime, DateTime>(v => v.ToUniversalTime(), v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

		var nullableDateTimeConverter = new ValueConverter<DateTime?, DateTime?>(
			v => v.HasValue ? v.Value.ToUniversalTime() : v,
			v => v.HasValue ? DateTime.SpecifyKind(v.Value, DateTimeKind.Utc) : v);

		foreach (var entityType in modelBuilder.Model.GetEntityTypes())
		{
			if (entityType.IsKeyless)
			{
				continue;
			}

			foreach (var property in entityType.GetProperties())
			{
				if (property.ClrType == typeof(DateTime))
				{
					property.SetValueConverter(dateTimeConverter);
				}
				else if (property.ClrType == typeof(DateTime?))
				{
					property.SetValueConverter(nullableDateTimeConverter);
				}
			}
		}
		base.OnModelCreating(modelBuilder);
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		optionsBuilder.UseSnakeCaseNamingConvention();
	}
}