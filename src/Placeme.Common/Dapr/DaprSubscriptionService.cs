﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Placeme.Common.Application.EventBus.Events;
using Placeme.Common.Identity;

using static Dapr.AppCallback.Autogen.Grpc.v1.TopicEventResponse.Types;

namespace Placeme.Common.Dapr;

public class DaprSubscriptionService
{
	private readonly IServiceProvider _serviceProvider;
	private readonly ICurrentUser _currentUser;
	private readonly ILogger<DaprSubscriptionService> _logger;

	public DaprSubscriptionService(ILogger<DaprSubscriptionService> logger,
		 IServiceProvider serviceProvider, ICurrentUser currentUser)
	{
		_logger = logger;
		_serviceProvider = serviceProvider;
		_currentUser = currentUser;
	}

	public async Task<TopicEventResponseStatus> InvokeCallback(Collection<Tuple<string, Type, MethodInfo>> callbacks, string pubsub, string topic, string data)
	{
		var callback = callbacks.FirstOrDefault(c => c.Item1 == $"{pubsub}:{topic}");
		if (callback == null)
		{
			_logger.LogError("Cannot handle incoming event {Pubsub}:{Topic} since there is no callback available." +
							 "Make sure that the DaprSubscriptionService is registered in the DI and the subscriptions are resolved.", pubsub, topic);
			return TopicEventResponseStatus.Retry;
		}

		var firstParam = callback.Item3.GetParameters().FirstOrDefault();
		if (firstParam != null)
		{
			var p = JsonSerializer.Deserialize(data, firstParam.ParameterType, new JsonSerializerOptions
			{
				PropertyNameCaseInsensitive = true
			});
			if (p is BaseIntegrationEvent integrationEvent)
			{
				if (integrationEvent.UserprofileId == Guid.Empty)
				{
					_logger.LogInformation("Empty Userprofile, using system user instead.");
					integrationEvent.UserprofileId = Constants.SYSTEMUSER;
				}

				_currentUser.UserprofileId = integrationEvent.UserprofileId;
				_currentUser.TenantId = integrationEvent.TenantId;
			}
			var sub = _serviceProvider.GetService(callback.Item2);
			var result = callback.Item3.Invoke(sub, new[] { p })!;
			// In case task is return we need to await it
			if (result is Task task)
			{
				await task.ConfigureAwait(false);
				var resultProperty = task.GetType().GetProperty("Result");
				result = resultProperty!.GetValue(task);
			}
			if (result == null)
			{
				return TopicEventResponseStatus.Success;
			}

			if (result.GetType().IsGenericType && result.GetType().GetGenericTypeDefinition() == typeof(Task<>))
			{
				if (result.GetType().GenericTypeArguments[0] == typeof(TopicEventResponseStatus))
				{
					return await (Task<TopicEventResponseStatus>)result;
				}

				await (Task)result;
				return TopicEventResponseStatus.Success;
			}

			return result.GetType() == typeof(TopicEventResponseStatus) ? (TopicEventResponseStatus)result : TopicEventResponseStatus.Success;
		}

		return TopicEventResponseStatus.Retry;
	}
}