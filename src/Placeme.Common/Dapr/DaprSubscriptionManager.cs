﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

using Dapr;
using Dapr.AppCallback.Autogen.Grpc.v1;

namespace Placeme.Common.Dapr;

public class DaprSubscriptionManager
{
	private readonly Collection<Tuple<string, Type, MethodInfo>> _callbacks = new();
	private readonly Collection<TopicSubscription> _topicSubscriptions = new();
	private readonly ICollection<Type> _daprSubscriptions;

	public DaprSubscriptionManager()
	{
		_daprSubscriptions = new List<Type>(AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(t => t.GetInterfaces().Contains(typeof(IDaprSubscription))));
		GetAllCallbacks();
		GetAllSubscriptions();
	}

	public Collection<Tuple<string, Type, MethodInfo>> GetAllCallbacks()
	{
		if (_callbacks.Count > 0)
		{
			return _callbacks;
		}
		foreach (var subscription in _daprSubscriptions)
		{
			foreach (var method in subscription.GetMethods())
			{
				var topicSubscription = method.GetCustomAttribute<TopicAttribute>();
				if (topicSubscription != null)
				{
					_callbacks.Add(new Tuple<string, Type, MethodInfo>($"{topicSubscription.PubsubName}:{topicSubscription.Name}",
						subscription, method));
				}
			}
		}
		return _callbacks;
	}

	public IEnumerable<TopicSubscription> GetAllSubscriptions()
	{
		if (_topicSubscriptions.Count > 0)
		{
			return _topicSubscriptions;
		}
		foreach (var subscription in _daprSubscriptions)
		{
			foreach (var method in subscription.GetMethods())
			{
				var topicSubscription = method.GetCustomAttribute<TopicAttribute>();
				if (topicSubscription != null)
				{
					var sub = new TopicSubscription
					{
						PubsubName = topicSubscription.PubsubName,
						Topic = topicSubscription.Name
					};
					_topicSubscriptions.Add(sub);
				}
			}
		}
		return _topicSubscriptions;
	}
}