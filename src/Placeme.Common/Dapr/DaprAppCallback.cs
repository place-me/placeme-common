﻿using System.Threading.Tasks;

using Dapr.AppCallback.Autogen.Grpc.v1;

using Google.Protobuf.WellKnownTypes;

using Grpc.Core;

namespace Placeme.Common.Dapr;

public sealed class DaprAppCallback : AppCallback.AppCallbackBase
{
	private readonly DaprSubscriptionService _daprSubscriptionService;
	private readonly DaprSubscriptionManager _daprSubscriptionManager;

	public DaprAppCallback(DaprSubscriptionService daprSubscriptionService, DaprSubscriptionManager daprSubscriptionManager)
	{
		_daprSubscriptionService = daprSubscriptionService;
		_daprSubscriptionManager = daprSubscriptionManager;
	}

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
	public override async Task<ListTopicSubscriptionsResponse> ListTopicSubscriptions(Empty request,
		ServerCallContext context)
	{
		var response = new ListTopicSubscriptionsResponse();
		response.Subscriptions.AddRange(_daprSubscriptionManager.GetAllSubscriptions());
		return response;
	}
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

	public override async Task<TopicEventResponse> OnTopicEvent(TopicEventRequest request, ServerCallContext context)
	{
		var callbacks = _daprSubscriptionManager.GetAllCallbacks();
		var status =
			await _daprSubscriptionService.InvokeCallback(callbacks, request.PubsubName, request.Topic,
				request.Data.ToStringUtf8());
		return new TopicEventResponse { Status = status };
	}
}