using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;

using Placeme.Common.Identity;

namespace Placeme.Common.Dapr;

public class DaprBaseService
{
	private readonly ICurrentUser _currentUser;

	public DaprBaseService(ICurrentUser currentUser)
	{
		_currentUser = currentUser;
	}

	protected static Dictionary<string, string> CreateGetAllQueryParams(int pageNumber, int pageSize, string sorting)
	{
		var queryParams = new Dictionary<string, string>();
		if (pageNumber > 0 && pageSize > 0)
		{
			queryParams.Add("PageNumber", pageNumber.ToString(CultureInfo.InvariantCulture));
			queryParams.Add("PageSize", pageSize.ToString(CultureInfo.InvariantCulture));
		}

		if (!string.IsNullOrWhiteSpace(sorting))
		{
			queryParams.Add("Sort", sorting);
		}

		return queryParams;
	}
	protected HttpRequestMessage AddContextHttpHeaders(HttpMethod method, string requestUri)
	{
		return AddContextHttpHeaders(method, new Uri(requestUri));
	}

	protected HttpRequestMessage AddContextHttpHeaders(HttpMethod method, Uri requestUri)
	{
		var identifiers = new List<string>();
		if (_currentUser.UserprofileId != Guid.Empty)
		{
			identifiers.Add(_currentUser.UserprofileId.ToString());
		}

		if (_currentUser.TenantId != Guid.Empty)
		{
			identifiers.Add(_currentUser.TenantId.ToString());
		}

		var request = new HttpRequestMessage(method, requestUri);
		if (identifiers.Count > 0)
		{
			request.Headers.Add("Placeme-Context", identifiers);
		}

		return request;
	}
}