const helper = require('./.devcontainer/base/semantic-release/helper');
let config = helper.createBaseConfig();
helper.addNugetSupport(config);

module.exports = config;
